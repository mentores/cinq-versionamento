<div class="cinq-jobs" id="#cinq-jobs">
	<div class="input-group">
		<input type="text" class="form-control" id="jobsfilter" placeholder="Busca ...">
		<span class="input-group-addon">
			<a href="" id="busca">
				<i class="fa fa-search" aria-hidden="true"></i>
			</a>
		</span>
	</div>

	<div>
		<ul class="altura-limite">
			<?php foreach ($vagas as $vaga) {
				$titulo =	$vaga->Descricao ." - "  .
						 	$vaga->Quantidade." vaga".
						 	($vaga->Quantidade == 1 ? "" : "s");

				// if(strlen($titulo) > $limitchar = 50)
				// 	$titulo = substr( $titulo, 0 , $limitchar ) . ' ...';
			?>
			<li>
            	<a class="alignright jobst" vaga-id="<?php echo $vaga->Codigo ;?>" > 
					<i class="fa fa-chevron-right"></i> 
				</a>
				<div class="titulo-vaga">
					<?php echo $titulo; ?> 
					
				</div>
                
                <template class="vaga-titu" vaga-id="<?php echo $vaga->Codigo ;?>" >
                	<?php echo $titulo ;?>
                </template>

				<template class="vaga-desc" vaga-id="<?php echo $vaga->Codigo ;?>" > 
					<div class="descricao-vaga">
						<?php echo str_replace("\n\r","<br>",$vaga->Perfil);?>
					</div>
				</template>
			</li>
			<?php } ?>
		</ul>
		<div class="panel panel-default">
            <div class="panel-heading">
            	<a class="view-jobs" href="#"><i class="voltar-jobs fa fa-chevron-left"></i></a>
            	<div class="view-titl"></div>
            </div>
            <div class="panel-body">
               	<div class="view-desc"></div>
            </div>
		</div>
    </div>
</div>
