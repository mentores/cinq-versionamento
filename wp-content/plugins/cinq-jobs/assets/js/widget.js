jQuery(function($){
	var p = ".cinq-jobs";
	var base_top = 0;
	var current_top = 0;
	$('#jobsfilter',p).keyup(function () {
		var rex = new RegExp($(this).val(), 'i');
		$('.cinq-jobs li').hide();
		$('.cinq-jobs li').filter(function () {
			return rex.test($(this).text());
		}).show();
	})
	
	$( "#busca",p).click(function( e ) {
		e.preventDefault();
	})

	$( ".titulo-vaga,.jobst",p).click(function( e ) {
		e.preventDefault();

		base_top 	= $(".altura-limite",p).offset().top;
		current_top = $("li:eq(0)",".altura-limite").offset().top;

		$(".altura-limite").scrollTop(0);
		var vaga_id 	= $(".jobst",$(this).parent()).attr("vaga-id");
		var vaga_desc	= $(".vaga-desc[vaga-id="+vaga_id+"]").html();
		var vaga_titu	= $(".vaga-titu[vaga-id="+vaga_id+"]").html();

		$(".view-desc").html(vaga_desc);
		$(".view-titl").html(vaga_titu);

		change_view();

	}).mouseover(function() {
		$(this).css("background-color","#e1ebef");
		$("a",this).css("color","#23527c");
	}).mouseout(function() {
		$("a",this).css("color","#337ab7");
		$(this).css("background-color","#eeeeee");
	});


	$(".view-jobs").click(function( e ){
		e.preventDefault();
		change_view();
		$(".altura-limite").scrollTop(current_top > base_top ? 0 : base_top - current_top);
	});

	function change_view(){
		
		$(".input-group",p).toggle();
		$("ul",p).toggle();
		$(".view-jobs",p).toggle();
		// $(".view-desc",p).toggle();
		$(".panel-default",p).toggle();
		
		
	}
})
