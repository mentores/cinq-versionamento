<?php
/*
  Plugin Name: CINQ Jobs
  Plugin URI: https://intra.cinq.com.br
  Description: Widget de vagas das cinq
  Version: 1.0.0.1
  Author: Patrick Facchin
  Author URI: https://www.patrickfacchin.com.br
  Text Domain: cinq-jobs
*/

if(!function_exists( 'is_admin' )){
	header('Status: 403 Forbidden');
	header('HTTP/1.1 403 Forbidden');
	exit();
}


// CWJ plugin version
if(!defined('CWJ_VER')) define('CWJ_VER', "1.0.0.1");
// CWJ plugin dir path
if(!defined('CWJ_DIR')) define('CWJ_DIR', plugin_dir_path( __FILE__ ));
// CWJ plugin dir URI
if(!defined('CWJ_URI')) define('CWJ_URI', plugin_dir_url ( __FILE__ ));
// CWJ webservice
if(!defined('CWJ_WS'))	define('CWJ_WS', "https://intranet.cinq.com.br/webservices");
// CWJ modulo
if(!defined('CWJ_MD'))	define('CWJ_MD', "/cinqvagas/cinqvagas.asmx?wsdl"); 


# Control class
class Cinq_Jobs {
	public function __construct() {
		add_action( 'widgets_init', [$this, 'widgets_init'] );
	}

	public function widgets_init() {
		register_widget( 'Cinq_Widgets_Jobs' );
	}
}


# Creating the widget 
class Cinq_Widgets_Jobs extends WP_Widget {

	/**
	 * Constructor
	 *
	 * @return void
	 **/
	public function __construct() {
		
		parent::__construct(
			'widget-cinq-jobs', 
			'Widget Cinq Jobs', 
			array( 'description' => 'Widget que mostra as vagas abertas.' ) 
		);

	}
	# Gerencia a Home
	public function widget( $args, $instance ) {

		wp_enqueue_script( 'cwj', CWJ_URI.'assets/js/widget.js'  , array( 'jquery' ), self::dq_version(), true);
		wp_enqueue_style ( 'cwj', CWJ_URI.'assets/css/widget.css', false, self::dq_version() );

		extract( $args );
		echo $before_widget;
		
		if(!empty( $instance['titulo']))
			echo 	$args['before_title'], 
					apply_filters( 'widget_title', $instance['titulo'] ),
					$args['after_title'];

		
		$params = [	'vagas' 	=> json_decode( self::getvagas())];

		self::load_template( "widget", $params );
		
		echo $after_widget;
	}

	public function update( $new_instance, $old_instance ) {
		// update logic goes here
		$updated_instance = $new_instance;
		return $updated_instance;
	}

	# Gerencia o Painel do Colaborador
	public function form( $instance ) {
		$titulo = isset( $instance[ 'titulo' ] ) ? $instance[ 'titulo' ] : '';

		$params = [	'titulo_id' 	=> $this->get_field_id  ( 'titulo' ),
						'titulo_name' 	=> $this->get_field_name( 'titulo' ),
						'titulo' 		=> esc_attr( $titulo )];
		
		self::load_template( "painel", $params );
   }

	private function load_template( $name, $params = null, $include = true ) {
		$template = CWJ_DIR . 'templates/'.$name.'.php';
		if(!is_null($params)) extract($params);
		include $template;
	}

	private function getVagas(){
		try{
			$client = null;
			$client =  new SoapClient(CWJ_WS.CWJ_MD);
		} catch (Exception $e){
			$client = null;
		}
		
		return is_null($client)? "null" : $client->GetVagasJSON()->GetVagasJSONResult;
	}

	private function dq_version() {
	   return WP_DEBUG ? time() : CWJ_VER;
	}
}


$GLOBALS['cwj'] = new Cinq_Jobs();