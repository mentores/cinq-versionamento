#!/usr/bin/env python
#coding: utf-8
import os
import glob
import sys
from time import sleep

files = ["jquery.form.min.js",
		 "textext.core.js",
		 "textext.plugin.autocomplete.js",
		 "textext.plugin.tags.js",
		 "transition.js",
		 "general.js",
		 "load-posts.js"]

path = '*.js'   
path_compress = '../js/'
file_compress = path_compress+'feature-request.js'
cmd = "yui-compressor --type js --line-break 120  --charset utf-8 --nomunge -o"




def printProgress (iteration, total, prefix = '', suffix = '', decimals = 1, barLength = 100):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        barLength   - Optional  : character length of bar (Int)
    """
    formatStr       = "{0:." + str(decimals) + "f}"
    percents        = formatStr.format(100 * (iteration / float(total)))
    filledLength    = int(round(barLength * iteration / float(total)))
    bar             = '█' * filledLength + '-' * (barLength - filledLength)
    sys.stdout.write('\r%s |%s| %s%s %s' % (prefix, bar, percents, '%', suffix)),
    sys.stdout.flush()
    if iteration == total:
        sys.stdout.write('\n')
        sys.stdout.flush()


i     = 0
l     = len(files)
rows, columns = os.popen('stty size', 'r').read().split()
columns = int(columns) - 30;
os.system("clear")
print "Gerando novo arquivo em "+file_compress

os.system("echo '' > "+file_compress)
for file in files:
	os.system (cmd+" "+path_compress+file+" "+file)
	# os.system ("cp "+file+" "+path_compress+file)
	os.system ("echo '//"+file+"\n' >> "+file_compress)
	os.system ("cat "+path_compress+file+" >> "+file_compress)
	os.system ("rm  "+path_compress+file)
	os.system ("echo '\n' >> "+file_compress)
	#sleep(0.0001)
	i += 1
	printProgress(i, l, prefix = 'Progress:', suffix = 'Complete', barLength = columns)