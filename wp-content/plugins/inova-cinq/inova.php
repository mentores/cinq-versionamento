<?php
/**
 * Feature request WordPress Plugin.
 *
 * @package   Feature-Request
 * @author    averta [averta.net]
 * @license   LICENSE.txt
 * @link      http://averta.net
 *
 *
 * Plugin Name:     Inova CINQ
 * Description:     Plugin do Inova CINQ, editado a partir do plugin <strong>Feature Request</strong>, disponivel no repositorio do Wordpress.
 * Version:         1.0
 * Author:          Yanne Moreira
 * Text Domain:     feature-request
 * Domain Path:     /languages/
 * Tested up to:    4.4
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

// Set some constants
define('AVFR_VERSION', '1.1.1.4');
// define('AVFR_VERSION', rand());
define('AVFR_DIR' , plugin_dir_path( __FILE__ ) );
define('AVFR_URL' , plugins_url( '', __FILE__ ) );

require_once( plugin_dir_path( __FILE__ ) . 'public/class-feature-request.php' );

register_activation_hook( __FILE__, array( 'Feature_Request', 'activate' ) );
register_deactivation_hook( __FILE__, array( 'Feature_Request', 'deactivate' ) );

add_action( 'plugins_loaded', array( 'Feature_Request', 'get_instance' ) );

if ( is_admin() ) {
	require_once( plugin_dir_path( __FILE__ ) . 'admin/class-avfr-admin.php' );
	add_action( 'plugins_loaded', array( 'AVFR_Admin', 'get_instance' ) );
}
