jQuery(document).ready(function() {
    //Variaveis vindas do PHP
    var lastSearch = null;
    var baseUrl = getTokens.base_url;
    var pluginUrl = getTokens.plugin_url;
    
    var today = moment().locale('pt-br').format('DD/MM');
    var inicioSemana = moment().locale('pt-br').startOf('isoWeek');
    var terminoDaSemana = moment().locale('pt-br').endOf('isoWeek');
    
    var inicioSemanaFormatada = moment(inicioSemana._d).locale('pt-br').format('DD/MM');
    var terminoDaSemanaFormatada = moment(terminoDaSemana._d).locale('pt-br').format('DD/MM');
    
	dataHj = moment().format("YYYY-MM-D");
	//console.log(dataHj);

	//Recupera o valor do token
	var refresh_token = getToken('refresh_token');
	var token = getToken('token_access');

	requisicaoAPI(dataHj);

	// Transforma a data em formato americano
	function formataDataAmericana(data) {
		split = data.split('/');
		data = split[2] + "-" + split[1] + "-" + split[0];
		//yyyy-mm-dd
		return data;
	}

	//Recupera o valor do cookie passado por parametro
	function getToken(type) {
		if (type == 'refresh_token') {
			return getTokens.refresh_token;
		} else if (type == 'token_access') {
			return getTokens.token_access;
		}
	}
	
	function createHttpRequest(method, url){
		var xhr = new XMLHttpRequest();
		
		if ("withCredentials" in xhr) {
			// XHR for Chrome/Firefox/Opera/Safari.
    		xhr.open(method, url, true);
  		} else if (typeof XDomainRequest != "undefined") {
			// XDomainRequest for IE.
    		xhr = new XDomainRequest();
    		xhr.open(method, url);
  		} else {
    		// CORS not supported.
    		xhr = null;
  		}
  		return xhr;
	}
	
	function refreshToken(token, refreshToken, baseUrl) {
		var baseUrl = baseUrl + '/connect/';
		var revokeUrl = baseUrl + 'token';

	  	var xhr = createHttpRequest('POST', revokeUrl);
	  	if (!xhr) {
		    alert('CORS não suportado!');
	    	return;
	  	}
        
        xhr.onload = function (e) {
            console.log(xhr.status);
            console.log(xhr.response);
        };
        
        //xhr.open("POST", revokeUrl);
        
        var data = {
            token: token,
            token_type_hint:"refresh_token",
            scope: "offline_access",
            grant_type: "refresh_token"
        };
        var body = "";
        for (var key in data) {
            if (body.length) {
                body += "&";
            }
            body += key + "=";
            body += encodeURIComponent(data[key]);
        }
        
        xhr.setRequestHeader("Authorization", "Bearer " + token);
        xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    	xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
    	xhr.setRequestHeader("Access-Control-Allow-Origin", "*");
        xhr.send(body);
	}

	function searchColaborador(searchCache){
		
		if(searchCache == true){
			var nomePesquisa = lastSearch;
		}else{
			var nomePesquisa = jQuery('.campo-busca').val();
		}

		if(nomePesquisa.length < 3 ){
			if(nomePesquisa.length>0 && nomePesquisa.length<3)
				jQuery('.campo-busca').val("").attr("placeholder","Digite o nome do colaborador. (min.3)").focus();
			if(!nomePesquisa.length)
				jQuery('.campo-busca').attr("placeholder","Digite o nome do colaborador").focus();

			jQuery('.input-busca').removeClass("acceptSearch").addClass("errorSearch");
			jQuery('.campo-busca').removeClass("acceptSearch").addClass("errorPlace");
		}else{
			acceptSearch();
			lastSearch = nomePesquisa;
			
			var data = '';
		
			requisicaoAPI(data, nomePesquisa);
		}
	}
	
	function acceptSearch(){
		jQuery('.input-busca').removeClass("errorSearch").addClass("acceptSearch")
		jQuery('.campo-busca').removeClass("errorPlace").addClass("acceptSearch")
	}

	jQuery('.campo-busca').keypress(function(){
		acceptSearch();
	})

	function requisicaoAPI(dataParam, nomePesquisa) {
		//Variavel do PHP
		var apiUrl = getTokens.url_api;
		var requestUrl = apiUrl + "/colaborador/GetAniversariantesBySemana?date=" + dataParam;
		var pesquisaPorNome = false;
		
		if (typeof(dataParam) !="undefined"){
			requestUrl = apiUrl + "/colaborador/GetAniversariantesBySemana?date=" + dataParam;
		}
			
		if (typeof(nomePesquisa) !="undefined"){
			if(nomePesquisa.length > 0){
				requestUrl = apiUrl + "/colaborador/GetAniversariantesBySemana?nome=" + nomePesquisa;
				pesquisaPorNome = true;
			}else{
				return;
			}
		}
			
		 
		//refreshToken(token, refresh_token, baseUrl);
		
		jQuery.ajax({
			url : requestUrl,
			dataType : 'json',
			headers : {
				'Authorization' : 'Bearer ' + token
			},
			success : function(data, status) {
				//return console.log("The returned data", data);
				// Cria um array para guardar a resposta da api
				var colaboradores = [];
				//seta a variavel que controla o objeto, vazio ou não
				var isEmpty = true;

				var id = 0;

				// Verifica se a API retornou algum colaborador
				for (var i in data ) {
					if (data.hasOwnProperty(i)) {
						isEmpty = false;
						break;
					}
				}
				
				if((isEmpty == true)&&(pesquisaPorNome == true)){
					var msg = "Sem resultados para essa busca :(";
				}else{
					var msg = "Sem aniversários para essa semana :(";
				}
				
				if (isEmpty) {
					var noResult = [];
					noResult.push("<p class='noResultImg'><img src='"+ pluginUrl +"/icones_aniversariantes.png'></p>");
					noResult.push("<p class='noResultTxt'>"+ msg +"</p>");
					jQuery(".widget_area_aniversariantes_dia_lista").empty().append(noResult);
					// Apaga a lista de colaboradores se a api nao retornar nada
				} else {
					//Retorno da API não está vazio
					// Faz um loop para cada colaborador retornado da api. Insere o nome dele em um <p> e adiciona no plugin.
					jQuery.each(data, function(key, val) {

						var dataNasc = moment(val.dataNascimento).locale('pt-br').format('DD/MM');
						
						colaboradores.push("<p class='widget_area_nome_colaborador' id='linha_" + id + "'><span class='nome-colaborador'>" + val.nome + "</span><span class='dia-nascimento'>" + dataNasc + "</span></p>");
						jQuery(".widget_area_aniversariantes_dia_lista").empty().append(colaboradores);
						id++;						
					});					
				}
				
				jQuery('.panel-grid-2').masonry();

				jQuery( ".dia-nascimento" ).each(function( index ) {
					if(jQuery( this ).text() == today ){
				  		jQuery( this ).parent().css("background-color","#BCE3BB");
				  	}
				});	
				
				jQuery('.campo-busca').focus();
			},
			error: function(e){
				var noResult = [];
				noResult.push("<p class='noResultImg'><img src='"+ pluginUrl +"/icones_aniversariantes.png'></p>");
				noResult.push("<p class='noResultTxt'>Sem aniversariantes nessa semana :( </p>");
				jQuery(".widget_area_aniversariantes_dia_lista").empty().append(noResult);
			}
		});
	}

	//inicializa campo data hoje
	//var dataHoje = moment.utc().locale('pt-br').format('DD [de] MMMM');

	jQuery('#widget_area_dataCompleta').html("<span class='inicio'>" + inicioSemanaFormatada + "</span> a <span class='termino'>" + terminoDaSemanaFormatada + "</span>");
	

	// Botão das setas pra frente e pra trás
	jQuery('.widget_area_button_arrow').on("click", function() {
		
		var direcaoSeta = jQuery(this).attr('id');
		//Pega o id da seta, para determinar se a data sera adicionada ou subtraida (right, left)
		
		var buscaAtiva = (jQuery('.area-datas').attr('class')).split(' ');
		
		

		if(buscaAtiva[1] == 'busca-ativa'){
			acceptSearch();
			var termino = moment(terminoDaSemana._d).locale('pt-br').format('DD/MM');
			jQuery('.termino').text(termino);
			jQuery('.area-datas').toggleClass('busca-ativa');
			jQuery('#button_arrow_right').removeClass('hidden');
			jQuery('.input-busca').addClass('hidden');
			jQuery('.btn-buscar').removeClass('hidden');
			jQuery('#widget_area_dataCompleta').removeClass('hidden');


			novaDataInicioSemana = moment(inicioSemana);
			var novaDataRequest = moment(novaDataInicioSemana._d).locale('pt-br').format('DD/MM/YYYY');
			var novaDataInicioSemanaCompleta = moment(novaDataInicioSemana._d).locale('pt-br').format('DD/MM');
			
			novaDataTerminoSemana = moment(terminoDaSemana);
			var novaDataTerminoSemanaCompleta = moment(novaDataTerminoSemana._d).locale('pt-br').format('DD/MM');
			
			inicioSemana = novaDataInicioSemana;
			terminoDaSemana = novaDataTerminoSemana;
			
		}else{

			if (direcaoSeta == "button_arrow_left") {

				novaDataInicioSemana = moment(inicioSemana).subtract(1,'week');
				var novaDataRequest = moment(novaDataInicioSemana._d).locale('pt-br').format('DD/MM/YYYY');
				var novaDataInicioSemanaCompleta = moment(novaDataInicioSemana._d).locale('pt-br').format('DD/MM');
				
				novaDataTerminoSemana = moment(terminoDaSemana).subtract(1,'week');
				var novaDataTerminoSemanaCompleta = moment(novaDataTerminoSemana._d).locale('pt-br').format('DD/MM');
				
				inicioSemana = novaDataInicioSemana;
				terminoDaSemana = novaDataTerminoSemana;
				// Subtrai a data em 1 dia e salva na variavel
			} else {
				novaDataInicioSemana = moment(inicioSemana).add(1,'week');
				var novaDataRequest = moment(novaDataInicioSemana._d).locale('pt-br').format('DD/MM/YYYY');
				var novaDataInicioSemanaCompleta = moment(novaDataInicioSemana._d).locale('pt-br').format('DD/MM');
				
				novaDataTerminoSemana = moment(terminoDaSemana).add(1,'week');
				var novaDataTerminoSemanaCompleta = moment(novaDataTerminoSemana._d).locale('pt-br').format('DD/MM');
				
				inicioSemana = novaDataInicioSemana;
				terminoDaSemana = novaDataTerminoSemana;
			}
		}	

		var dataRequest = formataDataAmericana(novaDataRequest);
		
		jQuery('#widget_area_dataCompleta').html('<span class="inicio">' + novaDataInicioSemanaCompleta + "</span> a <span class='termino'>" + novaDataTerminoSemanaCompleta + "</span>");

		//jQuery('.widget_area_data').val(novaDataInput);

		requisicaoAPI(dataRequest);
		
	});

	//Faz a Requisição assim que a data é escolhida
	jQuery("#datepicker").datepicker().on("changeDate", function(e) {
		var data1 = jQuery(this).val();

		// O formato da data contida na variável "data1" é dd/mm/yyyy
		dataInput = formataDataAmericana(data1);

		var DataCompleta = moment.utc(dataInput).locale('pt-br').format('DD [de] MMMM');
		jQuery('#widget_area_dataCompleta').text(DataCompleta);

		dataCompleta = new Date(dataInput);
		//console.log(moment.utc(dataCompleta).locale('pt-br').format('LL'));

		//Envia a requisicao a API
		requisicaoAPI(dataInput);
	});
	
	// Opcão da Busca de Aniversariantes
	jQuery('.btn-buscar').on("click", function(){
		jQuery('#widget_area_dataCompleta').addClass('hidden');
		jQuery('#button_arrow_right').addClass('hidden');
		jQuery('.area-datas').addClass('busca-ativa');
		jQuery('.input-busca').removeClass('hidden');
		jQuery('#campo-busca').focus();
		jQuery(this).addClass('hidden');
		var termino = moment(terminoDaSemana._d).format('DD/MM');
		jQuery('.termino').text(termino);

		searchColaborador(true);
		
	});
	
	jQuery('.icon_search').on("click", function(){
		searchColaborador();		
	});

	jQuery("#campo-busca").keypress(function(e) {
	    if(e.which == 13) {
	        searchColaborador();
	    }
	});
}); 