<?php
	/**
	 * @author Wilterson Garcia
	 * 
	 * @description Inicializa os scripts e stylesheets para o plugin.
	 * */
	
	//Registra todos os scripts (styles e scripts js)
	function register_scripts_adicionais_plugin_cinq(){
	    if ( ! is_admin() ){
		    // Registrando Styles
		    wp_register_style( 'widget_styles', plugins_url('aniversariantes/front_end/widget_styles.css', 'Aniversariantes Cinq'));
		 	wp_register_style( 'bootstrap-datepicker3.standalone.min', plugins_url('aniversariantes/front_end/bootstrap-datepicker3.standalone.min.css', 'Aniversariantes Cinq'));
			wp_register_style( 'font-awesome.min','https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css');
			
			//Register Scripts
			wp_register_script( 'widget_script', plugins_url( 'aniversariantes/front_end/widget_script.js', 'Aniversariantes Cinq' ), array( 'jquery', 'jquery-ui-accordion' ));
			wp_register_script( 'bootstrap.min', plugins_url( 'aniversariantes/front_end/bootstrap.min.js', 'Aniversariantes Cinq' ), array( 'jquery' ));
			wp_register_script( 'bootstrap-datepicker', plugins_url( 'aniversariantes/front_end/bootstrap-datepicker.js', 'Aniversariantes Cinq' ), array( 'jquery' ));
			wp_register_script( 'moment.min', plugins_url( 'aniversariantes/front_end/moment.min.js', 'Aniversariantes Cinq'), array( 'jquery' ));
			wp_register_script( 'moment-with-locales.min', plugins_url( 'aniversariantes/front_end/moment-with-locales.min.js', 'Aniversariantes Cinq' ), array( 'jquery' ));
		}
	}
	add_action( 'init', 'register_scripts_adicionais_plugin_cinq' );

	//insere os styles na página
	function enqueue_widget_styles_adicionais(){
		if ( ! is_admin() ){
			wp_enqueue_style( 'widget_styles' );
			wp_enqueue_style( 'bootstrap-datepicker3.standalone.min' );
			wp_enqueue_style( 'font-awesome.min' );
		}
	}
	add_action( 'wp_enqueue_scripts', 'enqueue_widget_styles_adicionais' );
	
		//Insere os js na página
	function enqueue_widget_scripts_adicionais(){
		if ( ! is_admin() ){
			wp_enqueue_script( 'bootstrap.min' );
			wp_enqueue_script( 'bootstrap-datepicker' );
			wp_enqueue_script( 'moment.min' );
			wp_enqueue_script( 'moment-with-locales.min' );
		}
	}
	add_action( 'wp_enqueue_scripts', 'enqueue_widget_scripts_adicionais' );

		
	/* AJAX */
	add_action( 'wp_enqueue_scripts', 'ajax_enqueue_scripts' );
	function ajax_enqueue_scripts() {
		
		wp_enqueue_script( 'getTokens', plugins_url( 'front_end/widget_script.js', __FILE__ ), array('jquery'), '1.0', true );
		
		wp_localize_script( 'getTokens', 'getTokens', array(
			'token_access' => get_user_meta(wp_get_current_user()->ID, 'token_access', true),
			'refresh_token' => get_user_meta(wp_get_current_user()->ID, 'refresh_token', true),
			'url_api' => get_option('wpoa_api_url'),
			'base_url' => get_option('wpoa_cinq_api_url'),
			'plugin_url' => plugins_url( 'aniversariantes/front_end/', 'Aniversariantes Cinq' )
		));
		
	}