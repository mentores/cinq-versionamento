<?php
/*
Plugin Name: Aniversariantes CINQ
Description: Plugin dos aniversariantes da semana.
Version: 1.0.1.3
Author: Wilterson Garcia
Author: Patrick Facchin
*/

include_once('plugin_aniversariantes_init.php');

class aniversariantes_widget extends WP_Widget {
    public function __construct(){
    	parent::__construct(
			'widget-aniversariantes', 
			'Widget Aniversariantes', 
			array( 'description' => 'Widget que mostra os aniversariantes do dia.' ) 
		);
    }
    
    /**
     * Front-end display of widget.
     *
     * @see WP_Widget::widget()
     * 
     * @param array $args     Widget arguments.
     * @param array $instance Saved values from database.
     */
    public function widget( $args, $instance ){
        
        extract( $args );
        
		echo $args['before_widget'];
		
		if ( ! empty( $instance['titulo'] ) ) {
			echo $args['before_title'] . apply_filters( 'widget_title', $instance['titulo'] ). $args['after_title'];
		}
		
		// Front-end widget with date, photo and name.        
        ?>
			<!-- <form class="form-inline widget_area_form_data" id="widget_data_form" method="get"> -->  
				<div class="widget_area_data_completa_p disable-select">
					<span class="area-datas">
						<span class="widget_area_button_arrow" id="button_arrow_left">
							<i class="fa fa-chevron-left"></i>
						</span>
						<span id="widget_area_dataCompleta" class="widget_area_dataComp"></span>
						<span class="widget_area_button_arrow" id="button_arrow_right">
							<i class="fa fa-chevron-right"></i>
						</span>
					</span>
					
					<span class="input-busca hidden">
						<input type="text" class="campo-busca" name="campo-busca" id="campo-busca" placeholder="Digite o nome do colaborador">
						<i class="fa fa-search icon_search"></i>
					</span>					
					
					<span class="btn-buscar">Busca <i class="fa fa-search"></i></span>
					
				</div>
				<div class="clearfix"></div>
				<!-- <p class="widget_area_data_p date">
					<input type="text" class="widget_area_data datepicker form-control" id="datepicker" readonly="readonly" value="<?php echo date('d/m/Y')?>">
				</p> -->						
			<!-- </form> -->
			<div class="widget_area_aniversariantes_dia_lista disable-select">
				
			</div>
			
        <?php

        //echo $message;
        echo $after_widget;
    }

    /**
      * Sanitize widget form values as they are saved.
      * 
      * @see WP_Widget::update()
      *
      * @param array $new_instance Values just sent to be saved.
      * @param array $old_instance Previously saved values from database.
      *
      * @return array Updated safe values to be saved.
      */
    public function update( $new_instance, $old_instance ){
        
        $instance = array();
		$instance['titulo'] = ( ! empty( $new_instance['titulo'] ) ) ? strip_tags( $new_instance['titulo'] ) : '';

		return $instance;
    }

    /**
      * Back-end widget form.
      *
      * @see WP_Widget::form()
      *
      * @param array $instance Previously saved values from database.
      */
    public function form( $instance ){

        $titulo = isset( $instance[ 'titulo' ] ) ? $instance[ 'titulo' ] : '';
        ?>
	        <p>
				<label for="<?= $this->get_field_id( 'titulo' ); ?>">Título</label> 
				<input class="widefat"
					id="<?= $this->get_field_id( 'titulo' ); ?>" name="<?= $this->get_field_name( 'titulo' ); ?>"
					type="text" value="<?= esc_attr( $titulo ); ?>"
				/>
			</p> 
    	<?php
    }
}

/* Register the widget */
add_action( 'widgets_init', function(){
     register_widget( 'aniversariantes_widget' );
});