<?php
 add_action('wp_login', 'add_roles_wp_login');

 define('URL_API', rtrim(get_option('wpoa_api_url'), '/'));

 function add_roles_wp_login( $user_login, $user = null){

	$current_user 	= wp_get_current_user();
	$user 			= new WP_USER($current_user);
 	$user_id 		= $current_user->ID;
 	
 	$_SESSION['user_id'] = $user_id;
	update_user_meta( $user_id, 'token_access' , $_SESSION['token'] );
	update_user_meta( $user_id, 'refresh_token', $_SESSION['refresh_token'] );
	
	$token = get_user_meta( $user_id, 'token_access', true );

	##############################################################################
	##############################################################################
	##############################################################################
	##############################################################################
	# ADMINISTRANDO ROLES OBSOLETAS PARA LIPAR AS MESMAS #########################
	##############################################################################
	# COLETA TODAS AS ROLES ANTIGAS ##############################################
	# TODO: APOS ESTABILIZACAO DAS NOVAS ROLES REMOVER ESSA PARTE DO CODIGO. ATT
	// $response = wp_remote_request(  URL_API."/Menu/GetByIdMenuPai" , 
	// 								[	'method' 	=> 'GET',
	// 								    'blocking' 	=> true,
	// 								    'headers' 	=> ['Authorization' => "Bearer $token"]]);
	
	// $body = json_decode( wp_remote_retrieve_body( $response ), TRUE );
	
	// # INIT 
	// $perfis 			= [	'elaboradores', 'verificadores', 'aprovadores', 'visualizadores'];
	// $old_roles 			= [];
	// $old_roles_static	= [	'Perfil Cinqnet','Dashboard colaborador','Sobre a Cinq (WP)','Comercial',
	// 					 	'Gamificação','Produtividade','Inovação'];
	
	// # TRATANDO ROLES COLETADAS MANUALMENTE
	// foreach ($old_roles_static as $item)
	// 	foreach ($perfis as $perfil) 
	// 		$old_roles[] = clearRole($item) . "_" . $perfil;
	

	// foreach ($body as $item)
	// 	foreach ($perfis as $perfil) 
	// 		$old_roles[] = clearRole($item['descricao']) . "_" . $perfil;

	
	// # REMOVE ROLES OBSOLETAS
	// foreach ($old_roles as $obsolete_role) {
	// 	remove_role( $obsolete_role );
	// 	$user->remove_role( $obsolete_role );
	// }
	/*
	$old_roles = [	'qualidadeelaboradores'			, 'qualidade_elaboradores'			,
					'qualidadeverificadores'		, 'qualidade_verificadores'			,
					'qualidadeaprovadores'			, 'qualidade_aprovadores'			,
					'qualidadevisualizadores'		, 'qualidade_visualizadores'		,
					'painelelaboradores'			, 'painel_elaboradores'				,
					'painelverificadores'			, 'painel_verificadores'			,
					'painelaprovadores'				, 'painel_aprovadores'				,
					'painelvisualizadores'			, 'painel_visualizadores'			,
					'sobreacinqelaboradores' 		, 'sobreacinq_elaboradores'			,
					'sobreacinqverificadores' 		, 'sobreacinq_verificadores'		,
					'sobreacinqaprovadores' 		, 'sobreacinq_aprovadores'			,
					'sobreacinqvisualizadores' 		, 'sobreacinq_visualizadores'		,
					'painelwordpresselaboradores'	, 'painelwordpress_elaboradores'	,
					'painelwordpressverificadores'	, 'painelwordpress_verificadores'	,
					'painelwordpressaprovadores'	, 'painelwordpress_aprovadores'		,
					'painelwordpressvisualizadores'	, 'painelwordpress_visualizadores'	];
		
	// # REMOVE ROLES OBSOLETAS
	foreach ($old_roles as $obsolete_role) {
		remove_role( $obsolete_role );
		$user->remove_role( $obsolete_role );
	}*/
	##############################################################################
	##############################################################################
	##############################################################################
	##############################################################################
	# COLETA TODAS AS ROLES ######################################################

	/*
	$response = wp_remote_request(  URL_API."/Menu/Get" , 
									[	'method' 	=> 'GET',
									    'blocking' 	=> true,
									    'headers' 	=> ['Authorization' => "Bearer $token"]]);
	
	$body = json_decode( wp_remote_retrieve_body( $response ), TRUE );
	
	
	# INIT 
	$new_roles 			= [];
	$perfis 			= ['elaboradores', 'verificadores', 'aprovadores', 'visualizadores'];

	# ROLES DE MODALIDADES
	foreach ([	'Funcionário'	, 
				'Cooperado'  	, 
				'Contratado' 	, 
				'Estagiário' 	,
				'CLT Cotas'  	, 
				'Sócio/Diretor' , 
				'Aprendiz'		] as $modalidade) {
		$modalidade = "Contrato Modalidade - ".$modalidade;

		$new_roles[] = [ 	0=> clearRole($modalidade)	,
							1=> $modalidade 			];	
	}


	# POPULA ROLES
	foreach (getMenuNivel($body) as $key => $menu){
		foreach ($perfis as $perfil) 
			$new_roles[] = [ 	0=> clearRole($menu.$perfil),
								1=> $menu." - ".ucfirst($perfil) ];

		foreach (getMenuNivel($body, $key) as $skey => $smenu){
			$ssnivel = getMenuNivel($body, $skey);

			if(count($ssnivel))
				foreach ($ssnivel as $sskey => $ssmenu)
					foreach ($perfis as $perfil) 
						$new_roles[] = [ 	0=> clearRole($menu.$smenu.$ssmenu.$perfil),
									 		1=> $menu." - ".$smenu." - ".$ssmenu ." - ".ucfirst($perfil) ];
			else 	foreach ($perfis as $perfil)
						$new_roles[] = [ 	0=> clearRole($menu.$smenu.$perfil), 
				                     		1=> $menu." - ".$smenu." - ".ucfirst($perfil)];
		}
	}
	
	

	foreach ($new_roles as $role) {
		
		remove_role($role[0]);

		switch ($role[0]) {
			case 'painelwordpressaprovadores'	:
			case 'painelwordpresselaboradores'	:
			case 'painelwordpressverificadores'	:
			case 'painelwordpressvisualizadores':
				$capability = [	'dashboard_bloqued' 			=> FALSE ,
								'upload_files'					=> TRUE ,
						       	'read'         					=> TRUE , // true allows this capability
						       	'edit_posts'   					=> TRUE ,
						       	'edit_others_posts'				=> TRUE ,
						       	'read_private_posts'			=> TRUE ,
						       	'edit_private_posts'			=> TRUE ,
						       	'delete_posts' 					=> TRUE , // Use false to explicitly deny
						       	'publish_posts' 				=> TRUE ,
						       	'edit_published_posts' 			=> TRUE ,
						       	'delete_published_posts'		=> TRUE ,
						       	'delete_private_posts' 			=> TRUE ,
						       	'delete_others_posts'			=> TRUE ,
						       	'edit_pages' 					=> TRUE ,
						       	'publish_pages' 				=> TRUE ,
						       	'delete_pages' 					=> TRUE ,
								'ap_delete_comment' 			=> TRUE ,
						       	'ap_delete_others_comment'		=> TRUE ,
						       	'ap_edit_comment'				=> TRUE ,
						       	'ap_edit_others_comment'		=> TRUE ,
						       	'ap_hide_others_comment'		=> TRUE ,
						       	'ap_new_comment'				=> TRUE ,
						       	'dwqa_can_delete_answer'		=> TRUE ,
						       	'dwqa_can_delete_comment'		=> TRUE ,
						       	'dwqa_can_delete_question'		=> TRUE ,
						       	'dwqa_can_edit_answer'			=> TRUE ,
						       	'dwqa_can_edit_comment'			=> TRUE ,
						       	'dwqa_can_edit_question'		=> TRUE ,
						       	'dwqa_can_post_answer'			=> TRUE ,
						       	'dwqa_can_post_comment'			=> TRUE ,
						       	'dwqa_can_post_question'		=> TRUE ,
						       	'dwqa_can_read_answer'			=> TRUE ,
						       	'dwqa_can_read_comment'			=> TRUE ,
						       	'dwqa_can_read_question'		=> TRUE ,
						       	'moderate_comments' 			=> TRUE ,
						       	'import' 						=> TRUE ,
						       	'export' 						=> TRUE ,
						       	'edit_others_pages'				=> TRUE ,
						       	'read_private_pages'			=> TRUE ,
						       	'delete_private_pages'			=> TRUE ,
						       	'delete_published_pages'		=> TRUE ,
						       	'delete_others_pages'			=> TRUE ,
						       	'edit_private_pages'			=> TRUE ,
						       	'edit_published_pages' 			=> TRUE ,
						       	'smartslider'					=> TRUE ,
						       	'add_yop_poll_votes'			=> TRUE ,
						       	'become_yop_poll_pro'			=> TRUE ,
						       	'clone_own_yop_polls'			=> TRUE ,
						       	'clone_own_yop_polls_templates' => TRUE ,
						       	'clone_yop_polls'				=> TRUE ,
						       	'clone_yop_polls_templates'		=> TRUE ,
						       	'delete_own_yop_polls'			=> TRUE ,
						       	'delete_own_yop_polls_templates'=> TRUE ,
						       	'delete_yop_polls'				=> TRUE ,
						       	'delete_yop_polls_logs'			=> TRUE ,
						       	'delete_yop_polls_templates'	=> TRUE ,
						       	'edit_own_yop_polls'			=> TRUE ,
						       	'edit_own_yop_polls_templates'	=> TRUE ,
						       	'edit_yop_polls'				=> TRUE ,
						       	'edit_yop_polls_templates'		=> TRUE ,
						       	'help_yop_poll_page'			=> TRUE ,
						       	'manage_yop_polls_bans'			=> TRUE ,
						       	'manage_yop_polls_imports'		=> TRUE ,
						       	'manage_yop_polls_options'		=> TRUE ,
						       	'reset_own_yop_polls_stats'		=> TRUE ,
						       	'reset_yop_polls_stats'			=> TRUE ,
						       	'view_own_yop_polls_logs'		=> TRUE ,
						       	'view_own_yop_polls_results'	=> TRUE ,
						       	'view_yop_polls_imports'		=> TRUE ,
						       	'view_yop_polls_logs'			=> TRUE ,
						       	'view_yop_polls_results'		=> TRUE ];
				break;
			case 'painelaprovadores'	:
			case 'painelelaboradores'	:
			case 'painelverificadores'	:
			case 'painelvisualizadores'	:
				$capability = [ 'dashboard_bloqued' 			=> TRUE ,
								'dwqa_can_read_comment' 		=> TRUE ,
								'dwqa_can_read_question' 		=> TRUE ,
								'dwqa_can_read_answer' 			=> TRUE ,
								'dwqa_can_post_question' 		=> TRUE ,
								'dwqa_can_post_comment' 		=> TRUE ,
								'dwqa_can_post_answer' 			=> TRUE ,
								'inova'							=> TRUE ];
				break;
			#############################################################
			# QUALIDADE
			case 'qualidadedocumentosdaqualidademanualdaqualidadeonisoelaboradores':
			case 'qualidadedocumentosdaqualidadeprocedimentosonisoelaboradores':
			case 'qualidadedocumentosdaqualidadeinstrucoesdetrabalhoonisoelaboradores':
			case 'qualidadedocumentosdaqualidadeferramentas,tecnicaseregrasonisoelaboradores':
			case 'qualidadedocumentosdaqualidadeglossarioelaboradores':
				$capability = [ 'dashboard_bloqued' 						=> false ,
								'edit_posts' 				 				=> true  ,
								'read' 										=> true  ,
								'docs-qualidade_edit_artefacts' 			=> true  ,
								'docs-qualidade_create_artefacts' 			=> true  ,
								// 'docs-qualidade_others_artefacts' 			=> false ,
								'docs-qualidade_edit_others_artefacts' 		=> true  ,
								'docs-qualidade_publish_artefacts' 			=> true  ,
								'docs-qualidade_edit_published_artefacts' 	=> true  , 
								// 'ow_view_reports' 							=> false ,
								// 'ow_view_others_inbox' 						=> false ,
								'ow_make_revision' 							=> true  ,
								'ow_make_revision_others' 					=> true  ,
								'ow_sign_off_step' 							=> true  ,
								'ow_view_workflow_history'	 				=> true  ,
								'ow_submit_to_workflow' 					=> true  ];
				break;
			case 'qualidadedocumentosdaqualidademanualdaqualidadeonisoverificadores':
			case 'qualidadedocumentosdaqualidadeprocedimentosonisoverificadores':
			case 'qualidadedocumentosdaqualidadeinstrucoesdetrabalhoonisoverificadores':
			case 'qualidadedocumentosdaqualidadeferramentas,tecnicaseregrasonisoverificadores':
			case 'qualidadedocumentosdaqualidadeglossarioverificadores':
				$capability = [ 'dashboard_bloqued' 						=> false ,
								'edit_posts' 				 				=> true  ,
								'read' 										=> true  ,
								'docs-qualidade_edit_artefacts' 			=> true  ,
								// 'docs-qualidade_create_artefacts' 			=> false ,
								'docs-qualidade_others_artefacts' 			=> true  , # !!!!!!
								'docs-qualidade_edit_others_artefacts' 		=> true  ,
								'docs-qualidade_publish_artefacts' 			=> true  ,
								'docs-qualidade_edit_published_artefacts' 	=> true  , 
								// 'ow_view_reports' 							=> false ,
								// 'ow_view_others_inbox' 						=> false ,
								// 'ow_make_revision' 							=> false ,
								// 'ow_make_revision_others' 					=> false ,
								'ow_sign_off_step' 							=> true  ,
								'ow_view_workflow_history'	 				=> true  ];
								// 'ow_submit_to_workflow' 					=> false ];
				break;
			case 'qualidadedocumentosdaqualidademanualdaqualidadeonisoaprovadores':
			case 'qualidadedocumentosdaqualidadeprocedimentosonisoaprovadores':
			case 'qualidadedocumentosdaqualidadeinstrucoesdetrabalhoonisoaprovadores':
			case 'qualidadedocumentosdaqualidadeferramentas,tecnicaseregrasonisoaprovadores':
			case 'qualidadedocumentosdaqualidadeglossarioaprovadores':
				$capability = [ 'dashboard_bloqued' 						=> false ,
								'edit_posts' 				 				=> true  , 
								'read' 										=> true  ,
								'docs-qualidade_edit_artefacts' 			=> true  ,
								// 'docs-qualidade_create_artefacts' 			=> false ,
								'docs-qualidade_others_artefacts' 			=> true  ,
								'docs-qualidade_edit_others_artefacts' 		=> true  ,
								'docs-qualidade_publish_artefacts' 			=> true  ,
								'docs-qualidade_edit_published_artefacts' 	=> true  , 
								'ow_view_reports' 							=> true  ,
								'ow_view_others_inbox' 						=> true  ,
								// 'ow_make_revision' 							=> false ,
								// 'ow_make_revision_others' 					=> false ,
								'ow_sign_off_step' 							=> true  ,
								'ow_view_workflow_history'	 				=> true  ];
								// 'ow_submit_to_workflow' 					=> false ];
				break;
				// Qualidade - Documentos da Qualidade - Manual da Qualidade on ISO - 
				// Qualidade - Documentos da Qualidade - Procedimentos on ISO - 
				// Qualidade - Documentos da Qualidade - Instruções de Trabalho on ISO - 
				// Qualidade - Documentos da Qualidade - Ferramentas, Técnicas e Regras on ISO - 
				// Qualidade - Documentos da Qualidade - Glossário - 
			#############################################################
			default:
				$capability = [ 'dashboard_bloqued' 			=> TRUE ];
				break;
		}
		
		add_role( 	$role[0], $role[1], $capability );

		#Delete as roles do usuario
		
		$user->remove_role( $role[0] );
	}
	*/
	##############################################################################
	# COLETA AS ROLES DO USARIO ##################################################
	
	
	#Delete as roles do usuario
	foreach ($user->roles as $user_role)
		$user->remove_role( $user_role );
	


	


	#PEGA MODALIDADE
	$url_ativ = URL_API."/ColaboradorContrato/GetAtivoByColaborador?idColaborador=";
	$response = wp_remote_request( 	$url_ativ .$_SESSION['WPOA']['ID_COLABORADOR'] , 
									[	'method' 	=> 'GET' ,
									    'blocking' 	=> true  ,
									    'headers' 	=> ['Authorization' => "Bearer $token"]]);
	$body = json_decode( wp_remote_retrieve_body( $response ), TRUE );

	$user->add_role( clearRole( "Contrato Modalidade - " . $body['contratoModalidadeDescricao'] ) );

	#PEGA ROLES
	$response = wp_remote_request(  URL_API."/Role/GetMenuWordPress" , 
									[	'method' 	=> 'GET' ,
									    'blocking' 	=> true  ,
									    'headers' 	=> ['Authorization' => "Bearer $token"]]);
	

	foreach (json_decode( wp_remote_retrieve_body( $response ), TRUE ) as $permissao){
		$user->add_role( clearRole($permissao) );
	}
		
	
	##############################################################################
 }

function getMenuNivel($menu, $nivel = null){

	$new_menu = [];

	foreach ($menu as $item)
		if($nivel == $item['idMenuPai'])
			$new_menu[$item['idMenu']]	= $item['descricao'];
	
	return $new_menu;
}

function clearRole($item){
	return strtolower(str_replace(array(',','', ' ', '(', ')','_','-','/','\\','{','}','[',']'),"",removeAcentos($item)));
}

function removeAcentos($string) {
	$from = "áàãâéêíóôõúüçÁÀÃÂÉÊÍÓÔÕÚÜÇ";
	$to = "aaaaeeiooouucAAAAEEIOOOUUC";
	
    $keys = array();
    $values = array();
    preg_match_all('/./u', $from, $keys);
    preg_match_all('/./u', $to, $values);
    $mapping = array_combine($keys[0], $values[0]);
    return strtr($string, $mapping);

}
