if(typeof(jQuery) != "undefined") jQuery(document).ready(function() {
	var interval  		= null;
	var initCheck		= false;
	var current_id		= null;
	
	if(typeof(window.crossover) != "undefined" && document.getElementById("test"))
		window.crossover.handshake('test', function(e){})

	
	
	window.evCheckSession 	= false;
	window.evCheckLogout 	= false;

	if(window.location.href.indexOf("wp-admin") == -1){
		checkSession();
		
		jQuery(window).on("blur focus", function(e) {
		    var prevType = jQuery(this).data("prevType");
		    


		    if (prevType != e.type) {   
		    	switch (e.type) {	
		            case "blur":
		                checkSession();
		                break;
		            case "focus":
		                checkSession();
		                break;
		            
		        }
		    }

		    jQuery(this).data("prevType", e.type);

		})
	}

	

	function checkSession(){
		setTimeout(	function(){
			initCheck = true;
			if(!window.evCheckSession && !window.evCheckLogout){
				window.evCheckSession = true;
				jQuery.ajax({
					url: "/wp-json/wp_sso_cinq/v1/onsession",
					dataType : 'json',
					success: function (data) {
						window.evCheckSession = false;
						if(data.onsession <= 1){
							logoutSession();
						}else{
							if(current_id == null)
								current_id = data.current_id;
							else if(current_id != data.current_id)
								logoutSession();

							clearInterval(interval);
							interval = setInterval(function(){
								checkSession();
							}, data.onsession * 1000);
						}
					},
					error: function (xhr, ajaxOptions, thrownError) {
						logoutSession();
					},
					timeout: 10000
				})
			}
		}, initCheck ? 0 : 2000);

	}

    function logoutSession(){
    	window.evCheckLogout = true;
    	jQuery("body").prepend(jQuery("<div>").addClass("backModalLoading").append(jQuery("<div>").addClass("loading")));
		jQuery(".backModalLoading").fadeIn("fast");	

    	setTimeout(	function(){
    					window.location.replace('/');
    				}, 2000);
	}

});
