<?php
define('URL_API', rtrim(get_option('wpoa_api_url'), '/'));

################################################################################################
# RETORNA TEMPO DE SESSAO 
add_action( 'rest_api_init', function () {
    register_rest_route( 'wp_sso_cinq/v1', '/onsession/', array(
        'methods' 	=> 'GET',
        'callback' 	=> 'onsession',
    ) );

    register_rest_route( 'wp_sso_cinq/v1', '/menuget/', array(
        'methods' 	=> 'GET',
        'callback' 	=> 'menuget',
    ) );

    // register_rest_route( 'wp_sso_cinq/v1', '/passwd_all/', array(
    //     'methods' 	=> 'GET',
    //     'callback' 	=> 'passwd_all',
    // ) );

    // register_rest_route( 'wp_sso_cinq/v1', '/logoutall/', array(
    //     'methods' 	=> 'GET',
    //     'callback' 	=> 'logoutall',
    // ) );
} );

function menuget() {
	
	if(!isset($_SESSION['token']) || empty($_SESSION['token']))
		return [ 'menu' => "Token inexistente!" ];

	$token = $_SESSION['token'];


	$response = wp_remote_request(  URL_API."/Menu/Get" , 
									[	'method' 	=> 'GET',
									    'blocking' 	=> true,
									    'headers' 	=> ['Authorization' => "Bearer $token"]]);
	$menu_get = $response;
	$menu_get['body'] = json_decode($menu_get['body']);
	$body = $menu_get['body'];
	
	


	# INIT 
	$new_roles 			= [];
	$perfis 			= ['elaboradores', 'verificadores', 'aprovadores', 'visualizadores'];

	# ROLES DE MODALIDADES
	foreach ([	'Funcionário'	, 
				'Cooperado'  	, 
				'Contratado' 	, 
				'Estagiário' 	,
				'CLT Cotas'  	, 
				'Sócio/Diretor' , 
				'Aprendiz'		] as $modalidade) {
		$modalidade = "Contrato Modalidade - ".$modalidade;

		$new_roles[] = [ 	0=> menuget_clearRole($modalidade)	,
							1=> $modalidade 			];	
	}

	
	

	# POPULA ROLES
	foreach (menuget_getMenuNivel($body) as $key => $menu){
		

		foreach ($perfis as $perfil) 
			$new_roles[] = [ 	0=> menuget_clearRole($menu.$perfil),
								1=> $menu." - ".ucfirst($perfil) ];

		foreach (menuget_getMenuNivel($body, $key) as $skey => $smenu){
			$ssnivel = menuget_getMenuNivel($body, $skey);

			if(count($ssnivel))
				foreach ($ssnivel as $sskey => $ssmenu)
					foreach ($perfis as $perfil) 
						$new_roles[] = [ 	0=> menuget_clearRole($menu.$smenu.$ssmenu.$perfil),
									 		1=> $menu." - ".$smenu." - ".$ssmenu ." - ".ucfirst($perfil) ];
			else 	foreach ($perfis as $perfil)
						$new_roles[] = [ 	0=> menuget_clearRole($menu.$smenu.$perfil), 
				                     		1=> $menu." - ".$smenu." - ".ucfirst($perfil)];
		}
	}

	
	
	foreach ($new_roles as $role) {
		
		remove_role($role[0]);

		switch ($role[0]) {
			case 'painelwordpressaprovadores'	:
			case 'painelwordpresselaboradores'	:
			case 'painelwordpressverificadores'	:
			case 'painelwordpressvisualizadores':
				$capability = [	'dashboard_bloqued' 			=> FALSE ,
								'upload_files'					=> TRUE ,
						       	'read'         					=> TRUE , // true allows this capability
						       	'edit_posts'   					=> TRUE ,
						       	'edit_others_posts'				=> TRUE ,
						       	'read_private_posts'			=> TRUE ,
						       	'edit_private_posts'			=> TRUE ,
						       	'delete_posts' 					=> TRUE , // Use false to explicitly deny
						       	'publish_posts' 				=> TRUE ,
						       	'edit_published_posts' 			=> TRUE ,
						       	'delete_published_posts'		=> TRUE ,
						       	'delete_private_posts' 			=> TRUE ,
						       	'delete_others_posts'			=> TRUE ,
						       	'edit_pages' 					=> TRUE ,
						       	'publish_pages' 				=> TRUE ,
						       	'delete_pages' 					=> TRUE ,
								'ap_delete_comment' 			=> TRUE ,
						       	'ap_delete_others_comment'		=> TRUE ,
						       	'ap_edit_comment'				=> TRUE ,
						       	'ap_edit_others_comment'		=> TRUE ,
						       	'ap_hide_others_comment'		=> TRUE ,
						       	'ap_new_comment'				=> TRUE ,
						       	'dwqa_can_delete_answer'		=> TRUE ,
						       	'dwqa_can_delete_comment'		=> TRUE ,
						       	'dwqa_can_delete_question'		=> TRUE ,
						       	'dwqa_can_edit_answer'			=> TRUE ,
						       	'dwqa_can_edit_comment'			=> TRUE ,
						       	'dwqa_can_edit_question'		=> TRUE ,
						       	'dwqa_can_post_answer'			=> TRUE ,
						       	'dwqa_can_post_comment'			=> TRUE ,
						       	'dwqa_can_post_question'		=> TRUE ,
						       	'dwqa_can_read_answer'			=> TRUE ,
						       	'dwqa_can_read_comment'			=> TRUE ,
						       	'dwqa_can_read_question'		=> TRUE ,
						       	'moderate_comments' 			=> TRUE ,
						       	'import' 						=> TRUE ,
						       	'export' 						=> TRUE ,
						       	'edit_others_pages'				=> TRUE ,
						       	'read_private_pages'			=> TRUE ,
						       	'delete_private_pages'			=> TRUE ,
						       	'delete_published_pages'		=> TRUE ,
						       	'delete_others_pages'			=> TRUE ,
						       	'edit_private_pages'			=> TRUE ,
						       	'edit_published_pages' 			=> TRUE ,
						       	'smartslider'					=> TRUE ,
						       	'add_yop_poll_votes'			=> TRUE ,
						       	'become_yop_poll_pro'			=> TRUE ,
						       	'clone_own_yop_polls'			=> TRUE ,
						       	'clone_own_yop_polls_templates' => TRUE ,
						       	'clone_yop_polls'				=> TRUE ,
						       	'clone_yop_polls_templates'		=> TRUE ,
						       	'delete_own_yop_polls'			=> TRUE ,
						       	'delete_own_yop_polls_templates'=> TRUE ,
						       	'delete_yop_polls'				=> TRUE ,
						       	'delete_yop_polls_logs'			=> TRUE ,
						       	'delete_yop_polls_templates'	=> TRUE ,
						       	'edit_own_yop_polls'			=> TRUE ,
						       	'edit_own_yop_polls_templates'	=> TRUE ,
						       	'edit_yop_polls'				=> TRUE ,
						       	'edit_yop_polls_templates'		=> TRUE ,
						       	'help_yop_poll_page'			=> TRUE ,
						       	'manage_yop_polls_bans'			=> TRUE ,
						       	'manage_yop_polls_imports'		=> TRUE ,
						       	'manage_yop_polls_options'		=> TRUE ,
						       	'reset_own_yop_polls_stats'		=> TRUE ,
						       	'reset_yop_polls_stats'			=> TRUE ,
						       	'view_own_yop_polls_logs'		=> TRUE ,
						       	'view_own_yop_polls_results'	=> TRUE ,
						       	'view_yop_polls_imports'		=> TRUE ,
						       	'view_yop_polls_logs'			=> TRUE ,
						       	'view_yop_polls_results'		=> TRUE ];
				break;
			case 'painelaprovadores'	:
			case 'painelelaboradores'	:
			case 'painelverificadores'	:
			case 'painelvisualizadores'	:
				$capability = [ 'dashboard_bloqued' 			=> TRUE ,
								'dwqa_can_read_comment' 		=> TRUE ,
								'dwqa_can_read_question' 		=> TRUE ,
								'dwqa_can_read_answer' 			=> TRUE ,
								'dwqa_can_post_question' 		=> TRUE ,
								'dwqa_can_post_comment' 		=> TRUE ,
								'dwqa_can_post_answer' 			=> TRUE ,
								'inova'							=> TRUE ];
				break;
			#############################################################
			# QUALIDADE
			case 'qualidadedocumentosdaqualidademanualdaqualidadeonisoelaboradores':
			case 'qualidadedocumentosdaqualidadeprocedimentosonisoelaboradores':
			case 'qualidadedocumentosdaqualidadeinstrucoesdetrabalhoonisoelaboradores':
			case 'qualidadedocumentosdaqualidadeferramentas,tecnicaseregrasonisoelaboradores':
			case 'qualidadedocumentosdaqualidadeglossarioelaboradores':
				$capability = [ 'dashboard_bloqued' 						=> false ,
								'edit_posts' 				 				=> true  ,
								'read' 										=> true  ,
								'docs-qualidade_edit_artefacts' 			=> true  ,
								'docs-qualidade_create_artefacts' 			=> true  ,
								// 'docs-qualidade_others_artefacts' 			=> false ,
								'docs-qualidade_edit_others_artefacts' 		=> true  ,
								'docs-qualidade_publish_artefacts' 			=> true  ,
								'docs-qualidade_edit_published_artefacts' 	=> true  , 
								// 'ow_view_reports' 							=> false ,
								// 'ow_view_others_inbox' 						=> false ,
								'ow_make_revision' 							=> true  ,
								'ow_make_revision_others' 					=> true  ,
								'ow_sign_off_step' 							=> true  ,
								'ow_view_workflow_history'	 				=> true  ,
								'ow_submit_to_workflow' 					=> true  ];
				break;
			case 'qualidadedocumentosdaqualidademanualdaqualidadeonisoverificadores':
			case 'qualidadedocumentosdaqualidadeprocedimentosonisoverificadores':
			case 'qualidadedocumentosdaqualidadeinstrucoesdetrabalhoonisoverificadores':
			case 'qualidadedocumentosdaqualidadeferramentas,tecnicaseregrasonisoverificadores':
			case 'qualidadedocumentosdaqualidadeglossarioverificadores':
				$capability = [ 'dashboard_bloqued' 						=> false ,
								'edit_posts' 				 				=> true  ,
								'read' 										=> true  ,
								'docs-qualidade_edit_artefacts' 			=> true  ,
								// 'docs-qualidade_create_artefacts' 			=> false ,
								'docs-qualidade_others_artefacts' 			=> true  , # !!!!!!
								'docs-qualidade_edit_others_artefacts' 		=> true  ,
								'docs-qualidade_publish_artefacts' 			=> true  ,
								'docs-qualidade_edit_published_artefacts' 	=> true  , 
								// 'ow_view_reports' 							=> false ,
								// 'ow_view_others_inbox' 						=> false ,
								// 'ow_make_revision' 							=> false ,
								// 'ow_make_revision_others' 					=> false ,
								'ow_sign_off_step' 							=> true  ,
								'ow_view_workflow_history'	 				=> true  ];
								// 'ow_submit_to_workflow' 					=> false ];
				break;
			case 'qualidadedocumentosdaqualidademanualdaqualidadeonisoaprovadores':
			case 'qualidadedocumentosdaqualidadeprocedimentosonisoaprovadores':
			case 'qualidadedocumentosdaqualidadeinstrucoesdetrabalhoonisoaprovadores':
			case 'qualidadedocumentosdaqualidadeferramentas,tecnicaseregrasonisoaprovadores':
			case 'qualidadedocumentosdaqualidadeglossarioaprovadores':
				$capability = [ 'dashboard_bloqued' 						=> false ,
								'edit_posts' 				 				=> true  , 
								'read' 										=> true  ,
								'docs-qualidade_edit_artefacts' 			=> true  ,
								// 'docs-qualidade_create_artefacts' 			=> false ,
								'docs-qualidade_others_artefacts' 			=> true  ,
								'docs-qualidade_edit_others_artefacts' 		=> true  ,
								'docs-qualidade_publish_artefacts' 			=> true  ,
								'docs-qualidade_edit_published_artefacts' 	=> true  , 
								'ow_view_reports' 							=> true  ,
								'ow_view_others_inbox' 						=> true  ,
								// 'ow_make_revision' 							=> false ,
								// 'ow_make_revision_others' 					=> false ,
								'ow_sign_off_step' 							=> true  ,
								'ow_view_workflow_history'	 				=> true  ];
								// 'ow_submit_to_workflow' 					=> false ];
				break;
				// Qualidade - Documentos da Qualidade - Manual da Qualidade on ISO - 
				// Qualidade - Documentos da Qualidade - Procedimentos on ISO - 
				// Qualidade - Documentos da Qualidade - Instruções de Trabalho on ISO - 
				// Qualidade - Documentos da Qualidade - Ferramentas, Técnicas e Regras on ISO - 
				// Qualidade - Documentos da Qualidade - Glossário - 
			#############################################################
			default:
				$capability = [ 'dashboard_bloqued' 			=> TRUE ];
				break;
		}

		add_role( 	$role[0], $role[1], $capability );
	}

	
	return [ 'menu' => $menu_get ]; 
} 

function menuget_clearRole($item){
	return strtolower(str_replace(array(',','', ' ', '(', ')','_','-','/','\\','{','}','[',']'),"",menuget_removeAcentos($item)));
}
function menuget_removeAcentos($string) {
	$from = "áàãâéêíóôõúüçÁÀÃÂÉÊÍÓÔÕÚÜÇ";
	$to = "aaaaeeiooouucAAAAEEIOOOUUC";
	
    $keys = array();
    $values = array();
    preg_match_all('/./u', $from, $keys);
    preg_match_all('/./u', $to, $values);
    $mapping = array_combine($keys[0], $values[0]);
    return strtr($string, $mapping);

}
function menuget_getMenuNivel($menu, $nivel = null){
	
	foreach ($menu as $item){
		$item = (array)$item;
		if($nivel == $item['idMenuPai'])
			$new_menu[$item['idMenu']]	= $item['descricao'];
	}
	
	return $new_menu;
}

function logoutall(){


	
	wp_clear_auth_cookie();
	session_destroy();
	if (isset($_SERVER['HTTP_COOKIE'])) {
	    $cookies = explode(';', $_SERVER['HTTP_COOKIE']);
	    foreach($cookies as $cookie) {
	        $parts 	= explode('=', $cookie);
	        $name 	= trim($parts[0]);
	        setcookie($name, '', time()-1000);
	        setcookie($name, '', time()-1000, '/');
	    }
	}
	
	return [ 'logout' 		=> "foi" ]; 

}

function onsession() {
	
	$onsession = (isset($_SESSION['WPOA']['EXPIRES_SSO']) ? $_SESSION['WPOA']['EXPIRES_SSO'] : 0 ) - time();
	
	if(!isset($_SESSION['token']) || empty($_SESSION['token'])){
		wp_clear_auth_cookie();
		session_destroy();
		if (isset($_SERVER['HTTP_COOKIE'])) {
		    $cookies = explode(';', $_SERVER['HTTP_COOKIE']);
		    foreach($cookies as $cookie) {
		        $parts 	= explode('=', $cookie);
		        $name 	= trim($parts[0]);
		        setcookie($name, '', time()-1000);
		        setcookie($name, '', time()-1000, '/');
		    }
		}
	}
	

	return [ 'onsession' 		=> $onsession < 0 ? 0 : $onsession,
			 'current_id' 		=> @$_SESSION['WPOA']['ID_COLABORADOR']]; 
} 

function passwd_rand( $length = 8 ) {
    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_-=+;:,.?";
    return substr( str_shuffle( $chars ), 0, $length );
}

function passwd_all(){


	require_once(ABSPATH."/wp-includes/class-phpass.php");
	global $wpdb; $ph = new PasswordHash(8, true );
	

	$results = $wpdb->get_results( "SELECT 	id 
									FROM   	wp_users 
									WHERE 	user_login not in ('time.cinq','master','intra-hom-admin','intra-dev-admin','intra-admin')");

	$count_users = count($results);

	foreach ($results as $reg) {
		$newpass = esc_sql($ph->HashPassword(passwd_rand()));
		$wpdb->query( "UPDATE wp_users SET user_pass = '$newpass' WHERE ID='$reg->id';");
	}
	
	return [ 'Total de usuarios afetados' => $count_users]; 

}

################################################################################################