<div class="cinq-points">
  <div class="row">
    <div class="col-lg-4 col-md-12"><h3 class="points-title">Meus pontos</h3></div>
    <div class="col-lg-4 col-md-12">
      <div class="points-mes">
        <div class="points"><?php echo $saldo;?></div>
        <div class="mes-ano"><?php echo $data_curr['mes_desc'],"/",$data_curr['ano'];?></div>
      </div>
    </div>
    <div class="col-lg-4 col-md-12 points-hist"><a href="/gamificacao/?Redirect=ColaboradorExtrato">Ver histórico completo</a></div>
  </div>
<div class="row"> 
  
    <div class="posicao-colaborador-logado-wrapper">
        <div class="col-md-12">
            <?php
              foreach ($ranking as $data) {     
                  if($data->colaboradorLogado){
            ?>
                      <div class="posicao-ranking"  >

                               <i class="<?php if(($data->posicao)<=3){echo "fa fa-trophy rank".$data->posicao;}?>" aria-hidden="true"></i> 
                              <?php echo " ".$data->posicao."º - ".$data->nomeColaborador;?> 
                              <span class="alignright"><?php echo $data->pontos;?></span>
                      </div>
              <?php }} ?>
          </div>
      </div>
       


  <div class="col-lg-5 col-md-12"><h4>Ranking mensal</h4></div>
	<div class="col-lg-7 col-md-12 text-right" style="padding-top: 10px;">
  <?php 
      $date = time();
      
      $day = date('d', $date);

         if($day>5){
             $date = strtotime("+1 months", $date);
          }
     		  echo " Data de fechamento: 05/".date('m', $date)."/".date('Y', $date);
    ?>
	</div>
</div>

<div class="ranking-cinq-points-wrapper">
  <ul>
    <?php foreach ($ranking as $data) {
            if(($data->posicao)<=3){
      ?>
      <li>
        <i class="fa fa-trophy rank<?php echo $data->posicao; ?>" aria-hidden="true"></i> 
     		<?php echo " ".$data->posicao."º - ".$data->nomeColaborador;?> <span class="alignright"><?php echo $data->pontos;?></span>
      </li>
      <?php }} 
        if(!count($ranking)){?>
          <li>
            <i class="fa fa-trophy rank" aria-hidden="true"></i> 
            Nenhum colaborador no ranking ainda!
          </li>
      <?php } ?>
  </ul>
  </div>
  <p class="text-right"><a href="/gamificacao/?Redirect=Ranking">Ver ranking completo</a></p>
</div>
