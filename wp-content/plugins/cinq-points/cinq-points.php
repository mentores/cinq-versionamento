<?php
/*
  Plugin Name: CINQ Points
  Plugin URI: https://intra.cinq.com.br
  Description: Widget com informações resumidas de gameficação.
  Version: 1.0.0.1
  Author: Patrick Facchin
  Author URI: https://www.patrickfacchin.com.br
  Text Domain: cinq-points
*/

if(!function_exists( 'is_admin' )){
	header('Status: 403 Forbidden');
	header('HTTP/1.1 403 Forbidden');
	exit();
}


// CWP plugin version
if(!defined('CWP_VER')) define('CWP_VER', "1.0.0.1");
// CWP plugin dir path
if(!defined('CWP_DIR')) define('CWP_DIR', plugin_dir_path( __FILE__ ));
// CWP plugin dir URI
if(!defined('CWP_URI')) define('CWP_URI', plugin_dir_url ( __FILE__ ));
// CWP webservice
if(!defined('CWP_WS'))	define('CWP_WS', rtrim(get_option('wpoa_api_url'), '/'));
// CWP modulo
if(!defined('CWP_MD'))	define('CWP_MD', "/ColaboradorExtrato/GetRankingMensal"); 


# Control class
class Cinq_Points {
	public function __construct() {
		add_action( 'widgets_init', [$this, 'widgets_init'] );
	}

	public function widgets_init() {
		register_widget( 'Cinq_Widgets_Points' );
	}
}


# Creating the widget 
class Cinq_Widgets_Points extends WP_Widget {

	/**
	 * Constructor
	 *
	 * @return void
	 **/
	public function __construct() {
		
		parent::__construct(
			'widget-cinq-points', 
			'Widget Cinq Points', 
			array( 'description' => 'Widget com informações resumidas de gameficação.' ) 
		);

	}
	# Gerencia a Home
	public function widget( $args, $instance ) {

		wp_enqueue_script( 'cwp', CWP_URI.'assets/js/widget.js'  , array( 'jquery' ), self::dq_version(), true);
		wp_enqueue_style ( 'cwp', CWP_URI.'assets/css/widget.css', false, self::dq_version() );

		extract( $args );
		echo $before_widget;
		
		if(!empty( $instance['titulo']))
			echo 	$args['before_title'], 
					apply_filters( 'widget_title', $instance['titulo'] ),
					$args['after_title'];

		self::load_template( "widget", self::getDados() );
		
		echo $after_widget;
	}

	public function update( $new_instance, $old_instance ) {
		// update logic goes here
		$updated_instance = $new_instance;
		return $updated_instance;
	}

	# Gerencia o Painel do Colaborador
	public function form( $instance ) {
		$titulo = isset( $instance[ 'titulo' ] ) ? $instance[ 'titulo' ] : '';

		$params = [	'titulo_id' 	=> $this->get_field_id  ( 'titulo' ),
					'titulo_name' 	=> $this->get_field_name( 'titulo' ),
					'titulo' 		=> esc_attr( $titulo )];
		
		self::load_template( "painel", $params );
   }

	private function load_template( $name, $params = null, $include = true ) {
		$template = CWP_DIR . 'templates/'.$name.'.php';
		if(!is_null($params)) extract($params);
		include $template;
	}

	private function getDados(){
		$dados = [];
		$dados['saldo']   	= null;
		$dados['ranking'] 	= null;
		$dados['data_curr']	= null;

		// CWP token
		if(!defined('CWP_TK'))	define('CWP_TK', get_user_meta( wp_get_current_user()->ID, 'token_access', true )); 
		
		if(isset($_SESSION) && count($_SESSION)){
					
			$dia_semana = date("W") - date("W", strtotime(date("Y-m-01", time())))+2;

			$data_curr = [];
			$data_curr['mes'] = $dia_semana === 1 ? date("m")-1 : date("m");
			$data_curr['ano'] = $dia_semana === 1 && date("m") === 1 ? date("Y")-1 : date("Y");
			switch (TRUE) {
		        case 1  == $data_curr['mes']:    $mes = "Janeiro";     break;
		        case 2  == $data_curr['mes']:    $mes = "Fevereiro";   break;
		        case 3  == $data_curr['mes']:    $mes = "Março";       break;
		        case 4  == $data_curr['mes']:    $mes = "Abril";       break;
		        case 5  == $data_curr['mes']:    $mes = "Maio";        break;
		        case 6  == $data_curr['mes']:    $mes = "Junho";       break;
		        case 7  == $data_curr['mes']:    $mes = "Julho";       break;
		        case 8  == $data_curr['mes']:    $mes = "Agosto";      break;
		        case 9  == $data_curr['mes']:    $mes = "Setembro";    break;
		        case 10 == $data_curr['mes']:    $mes = "Outubro";     break;
		        case 11 == $data_curr['mes']:    $mes = "Novembro";    break;
		        case 12 == $data_curr['mes']:    $mes = "Dezembro";    break; 
		 	}
				
			$data_curr['mes_desc'] = substr($mes,0,3);
			
			$dados['data_curr'] = $data_curr;
			 
		    

		    ########################################
		    # RANKING
		    $url_param = CWP_WS.CWP_MD."?mes=".$data_curr['mes']."&ano=".$data_curr['ano'];
		    $response = wp_remote_request(  $url_param , 
		                 [  'method'   => 'GET',
		                    'blocking' => true,
		                    'headers'  => ['Authorization' => "Bearer ".CWP_TK]] );
		    
		    $dados['ranking'] = json_decode($response['body']);

		    foreach ($dados['ranking'] as $data) {
		    	if($data->idColaborador == $_SESSION['WPOA']['ID_COLABORADOR']){
		    		$dados['saldo']  		= $data->pontos;
		    		$dados['posicao'] 		= $data->posicao;
		    		$dados['id_colaborador']= $data->idColaborador;

		    		break;
		    	}
		    }

		    ########################################
		}

		return $dados;
	}

	private function dq_version() {
	   return WP_DEBUG ? time() : CWP_VER;
	}
}


$GLOBALS['cwp'] = new Cinq_Points();