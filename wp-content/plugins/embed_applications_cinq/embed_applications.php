<?php
/* 
Plugin Name: Embed Applications CINQ
Version: 1.0
Author: Wilterson Garcia
Description: Plugin para embarcar aplicações .NET em widgets no Dashboard da CINQ
*/

//require_once("embed_applications_init.php");
require_once("embed_applications_custom_posts.php");

if (!class_exists("embed_applications")) {
	class embed_applications {
		var $adminOptionsName = "embed_applications_adminOptions";

	    function init() {
	    	$this->getAdminOptions();
	    }
	
	    function activate() {

	    }

	
	    function loadLanguage (){
    		$plugin_dir = basename(dirname(__FILE__));
	        load_plugin_textdomain( 'cinqtextdomain', null, $plugin_dir );
	    }
	
	    function late_iframe_loader($atts){
			$options = get_option( "embed_applications_adminOptions" );
			
			extract(shortcode_atts(array(
            	'height' => $options['basic_height'],'width' => $options['basic_width'],'frameborder' => '0','scrolling'=>'auto', 'src'=>'',
                'longdesc'=>'','marginheight'=>'0','marginwidth'=>'0', 'name'=>'','click_words'=>'','click_url'=>'', 'class'=>'',
           	), $atts));
			
            $args = array('height' => $height,'width' => $width,'frameborder' => $frameborder,
            	'longdesc'=>$longdesc,'marginheight'=>$marginheight,'marginwidth'=>$marginwidth, 'name'=>$name,'click_words'=>$click_words,'click_url'=>$click_url,
                'scrolling'=>$scrolling, 'src'=>$src, 'class'=>$class);
				
            $html = $this->do_iframe_script($args);
            return $html;
	    }
	
	    function printAdminPage() {
        	//require_once('admin-page.php');
	    }//End function printAdminPage()
	
	
	    function load_widgets() {
    		register_widget( 'embed_widget' );
	    }
	}
}

/*
 * includes the code for the iframe widget
 * 
 * */
 
require_once('embed-widget.php');

/*
 * setup new instance of plugin
 * 
 * */
 
if (class_exists("embed_applications")) {
	$cons_embed_applications = new embed_applications();
}

/*
 * Actions and Filters
 * 
 * */
if (isset($cons_embed_applications)) {
	
	/* 
	 * Actions
	 * setup widgets
	 * Initialize the admin panel
	 * 
	 * */
	if (!function_exists("embed_applications_ap")) {
		function embed_applications_ap() {
			global $cons_embed_applications;
			
			if (!isset($cons_embed_applications)) {
				return;
			}
			
			if (function_exists('add_options_page')) {
        		//add_options_page('Embed Aplications', 'Embed Applications CINQ', 'manage_options', basename(__FILE__), array(&$cons_embed_applications, 'printAdminPage'));
			}
		}
	}

	//add_action('admin_menu', 'embed_applications_ap',1); //admin page
	add_action('widgets_init',array(&$cons_embed_applications, 'load_widgets'),1); // loads widgets
	add_action('init',array(&$cons_embed_applications, 'loadLanguage'),1);  // add languages
	//add_shortcode('embed_application_shortcode', array(&$cons_embed_applications,'late_iframe_loader'),1); // setup shortcode [iframe_loader] basic
	register_activation_hook( __FILE__, array(&$cons_embed_applications, 'activate') );
}

/* 
 * template tags
 * 
 * */

// function add_embed_applications($args){
	// $iframe = new embed_applications();
	// echo $iframe->do_iframe_script($args);
// }

/*
 * End of Plugin Script
 * */
?>
