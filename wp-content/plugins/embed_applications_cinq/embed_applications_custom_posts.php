<?php

/**
 * Adicionamos uma acção no inicio do carregamento do WordPress
 * através da função add_action( 'init' )
 */
add_action( 'init', 'create_post_type_applications' );

/**
 * Esta é a função que é chamada pelo add_action()
 */
function create_post_type_applications() {

    /**
     * Labels customizados para o tipo de post
     * 
     */
    $labels = array(
	    'name' => _x('Aplicações', 'post type general name'),
	    'singular_name' => _x('Aplicação', 'post type singular name'),
	    'add_new' => _x('Adicionar Nova', 'film'),
	    'add_new_item' => __('Adicionar Nova Aplicação'),
	    'edit_item' => __('Editar Aplicação'),
	    'new_item' => __('Nova Aplicação'),
	    'all_items' => __('Todas as Aplicações'),
	    'view_item' => __('Ver Aplicação'),
	    'search_items' => __('Pesquisar Aplicação'),
	    'not_found' => __('Nenhuma Aplicação Encontrada'),
	    'not_found_in_trash' => __('Nenhuma Aplicação Encontrada na Lixeira'),
	    'parent_item_colon' => '',
	    'menu_name' => 'Aplicações CINQ'
    );
    
    /**
     * Registramos o tipo de post film através desta função
     * passando-lhe os labels e parâmetros de controlo.
     */
    register_post_type( 'embed_applications', array(
	    'labels' => $labels,
	    'public' => true,
	    'publicly_queryable' => true,
	    'show_ui' => true,
	    'show_in_menu' => true,
	    'capability_type' => array('embed_applications','posts'),
	    'has_archive' => true,
	    'hierarchical' => false,
	    'menu_position' => null,
	    'register_meta_box_cb' => 'aplicacoes_meta_box',
	    'menu_icon' => 'dashicons-media-code',
	    'supports' => array('title')
    ));
	
	flush_rewrite_rules();
}

/**
 * Adiciona campos personalizados (Meta Boxes) no Custom Post (Aplicações CINQ)
 * 
 * */
function aplicacoes_meta_box(){
	add_meta_box('url_embed', __('Url da Aplicação'), 'meta_box_url_embed', array('embed_applications','post'), 'advanced', 'high');
}

function meta_box_url_embed(){
      global $post;
      $metaBoxValor = get_post_meta( get_the_ID(), 'url_aplicacao', true); 
	?>        
    <label for="inputValorMeta">Url: </label>
    <input type="url" required="required" name="url_aplicacao" class="large-text" placeholder="Url da Aplicação (incluindo o http://)" id="inputValorMeta" value="<?php echo $metaBoxValor; ?>" />
	<?php
}

/**
 * Salva o custom field na tabela
 * */
add_action('save_post_embed_applications', 'save_aplicacao');
    
function save_aplicacao(){
    global $post;
	$url = isset($_POST['url_aplicacao']) ? $_POST['url_aplicacao'] : '';
	update_post_meta(get_the_ID(), 'url_aplicacao', $url );
}

/**
 * Capabilities docs-qualidade
 * 
 * */
 
 function add_caps() {
	// administrador	
	$roleAdm = get_role( 'administrator' );
	
	$roleAdm->add_cap( 'publish_embed_applications' );
	$roleAdm->add_cap( 'publish_others_embed_applications' );
	$roleAdm->add_cap( 'edit_embed_applications' );
	$roleAdm->add_cap( 'edit_others_embed_applications' );
	$roleAdm->add_cap( 'delete_embed_applications' );
	$roleAdm->add_cap( 'delete_others_embed_applications');
	$roleAdm->add_cap( 'read_private_embed_applications' );
	$roleAdm->add_cap( 'edit_embed_applications' );
	$roleAdm->add_cap( 'read_embed_applications' );
	$roleAdm->add_cap( 'edit_embed_applications' );
}
add_action( 'admin_init', 'add_caps');
