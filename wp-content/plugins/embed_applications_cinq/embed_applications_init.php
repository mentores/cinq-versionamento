<?php
	/**
	 * @author Wilterson Garcia
	 * 
	 * @description Inicializa os scripts e stylesheets para o plugin.
	 * */
	
	//Registra todos os scripts (styles e scripts js)
	function register_scripts_embed_cinq(){
	    if ( ! is_admin() ){
			wp_register_script( 'reconhece_nav', plugins_url( 'reconhece_nav.js', __FILE__ ), array( 'jquery' ));
			wp_register_script( 'altura_auto', plugins_url( 'altura_auto.js', __FILE__ ), array( 'jquery' ));
		}
	}
	add_action( 'init', 'register_scripts_embed_cinq' );
	
	//Insere os .js na página
	function enqueue_scripts_embed_cinq(){
		if ( ! is_admin() ){
			wp_enqueue_script( 'reconhece_nav' );
			wp_enqueue_script( 'altura_auto' );
		}
	}
	add_action( 'wp_enqueue_scripts', 'enqueue_scripts_embed_cinq' );