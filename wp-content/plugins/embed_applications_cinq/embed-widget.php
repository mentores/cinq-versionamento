<?php

class embed_widget extends WP_Widget {
	
	public function __construct(){
        parent::__construct(
        	'embed_applications_cinq', // Base ID
            //'Embed Applications CINQ',
            __( 'Widget Embed Applications CINQ', 'cinqtextdomain' ), // Name
            array(
                'classname'   => 'embed_applications_CINQ', 
                'description' => __( 'Inclua aplicações diretamente no seu dashboard através de widgets.', 'cinqtextdomain' )
            )
        );

        load_plugin_textdomain( 'cinqtextdomain', false, basename( dirname( __FILE__ ) ) . '/languages' );
    }
	
    function widget( $args, $instance ){
		
		extract($args, EXTR_SKIP);
	    $title = empty($instance['title']) ? ' ' : apply_filters('widget_title', $instance['title']);
	    $url = empty($instance['widget']) ? '' : $instance['widget'];
		
    	/* Before widget (defined by themes).*/ 
    	echo $before_widget;
    	 
		/* Title of widget (before and after defined by themes).*/ 
    	if ( $title ){
        	echo $before_title . $title . $after_title;
		}
		 
        // FRONT DO WIDGET 
		?>
			<!-- <iframe src="<?php echo $url?>"></iframe> -->
			<div class="frame" value="<?php echo $url ?>"></div>	
		<?php

        echo $after_widget;
    }
	
    function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		$instance['widget'] = ( ! empty( $new_instance['widget'] ) ) ? strip_tags( $new_instance['widget'] ) : '';
		
        return $instance;
    }

	function form( $instance ) {
		$instance = wp_parse_args( (array) $instance, array( 'title' => '' ) );
		$title = isset($instance['title'])? $instance['title'] : '';
    	$widget = isset($instance['widget'])? $instance['widget'] : '';
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text"  value="<?php echo $instance['title'] ?>" />
		</p>

		<p>
			<label for="<?php echo $this->get_field_id(' widget ')?>"><?php _e('Widget Desejado: ','iframeLoader'); ?></label>
		    <select id="widget_embed_applications" name="<?php echo $this->get_field_name( 'widget' )?>" style="width: 90%;" required="required">
	    		<option value="">Selecione a Aplicação</option>
	    		
	    		<?php
		    		global $post;
					$listings = new WP_Query();
					$listings->query( 'post_type=embed_applications' );
					if($listings->found_posts > 0) {
						while ($listings->have_posts()) {
							$listings->the_post();
							$postId = get_the_ID();
							
							$urlAplicacao = get_post_meta( $postId, 'url_aplicacao', TRUE );
							?>
								<option value="<?php echo $urlAplicacao ?>" <?php echo ($widget==$urlAplicacao)?'selected':''; ?> ><?php echo the_title()?></option>
							<?php
						}
						wp_reset_postdata();
					}
		    	?>
	    	</select>		    
	    </p>
	    <?php
    }
}
?>
