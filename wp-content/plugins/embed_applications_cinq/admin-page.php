<?php
    
	//$devOptions = $this->getAdminOptions();
	//print_r($devOptions);
    
    if (isset($_POST['widget_name']) && isset($_POST['widget_url'])) {//save option changes
    
    	global $wpdb;
	
		$widget_name = strtolower( $_POST['widget_name'] );
		$widget_url =  strtolower( $_POST['widget_url'] );
		
		$table_name = $wpdb->prefix . 'embed_applications_cinq_options';
		
		$wpdb->insert(
			$table_name, 
			array(
				'widget' => $widget_name, 
				'url' => $widget_url,
				'time' => current_time( 'mysql' )
			)
		);
	?>
	<div class="updated"><p><strong>Widget Cadastrado</strong></p></div>
	<?php } ?>
	
	<style type="text/css">

		.embed_applications_section_settings{
			background-color: #FFF;
			margin-bottom: 1.5em;
			border: 1px solid #ddd;
		}
		.form-padding{
			padding: 0 1em 1em 1em;
			z-index: 1;
			position: relative;
		}
    </style>
	
	<form method="post" action="options-general.php?page=embed_applications.php">
		<div class="wrap">
        	<?php wp_nonce_field('update-options'); ?>
        	
            <h1><?php _e('Embed Applications CINQ Settings','iframe-loader'); ?></h1>
            
            <div class="embed_applications_section_settings" id="embed_applications_section_new">	            
	            <div class="form-padding" id="embed_applications_settings_panel">
	            	
	            	<h2><?php _e('Cadastre novos widgets para as aplicações','iframe-loader'); ?></h2>
		            <p><?php _e('Cada widget deve ter um nome e url únicos','iframe-loader'); ?></p>
		                
		            <table>
		            	<thead>
		                	<tr>
		                		<th><?php _e('Nome do Widget','iframe-loader'); ?></th>
		                		<th><?php _e('URL da Aplicação','iframe-loader'); ?></th>
		                    </tr>
		                </thead>
		                <tbody>
		                	<tr>
	                			<td><input style="width: 100%" type="text" name="widget_name" required="required"/></td>
		                        <td><input style="width: 100%" type="url" name="widget_url" required="required"/></td>
		                    </tr>
		                </tbody>
		            </table>
		            <br /><br />			
		            <?php submit_button('Cadastrar Widget');?>
				</div>
			</div>
			<div class="embed_applications_section_settings" id="embed_applications_section_cadastred">
				<div class="form-padding" id="embed_applications_settings_panel">
	            	
	            	<h2><?php _e('Widgets cadastrados','iframe-loader'); ?></h2>

		            <table class="wp-list-table widefat fixed striped">
		            	<thead>
	            			<tr>
		            			<th scope="col"><b>Widget</b></th>
		            			<th scope="col"><b>URL</b></th>
		            		</tr>
		            	</thead>
		                <tbody>
		                	<?php
	                			global $wpdb;
								
								$table_name = $wpdb->prefix . "embed_applications_cinq_options";
		                		$widgets = $wpdb->get_results( "SELECT widget, url FROM $table_name", ARRAY_A );

		                	?>
		                    <?php foreach ( $widgets as $linha ) { ?>
		                    	
	                    		<tr>
	                    			<?php foreach ( $linha as $key => $value ) { ?>
	                    			
	                        			<td><?php echo $value ?></td>
	                        		
	                        		<?php } ?> 
	                    		</tr>		                    
		                    <?php } ?>
		                </tbody>
		            </table>
				</div>
			</div>
		</div>
	</form>