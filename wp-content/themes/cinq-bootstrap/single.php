<?php get_header(); ?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>


	<h1><?php the_title(); ?></h1>
<div class="col-md-8">
<?php the_time('l, j F, Y, g:i') ?>
	<?php the_content(); ?>

<hr>
<?php comments_template(); ?> 
<?php endwhile; else: ?>
	<p><?php _e('Sorry, this post does not exist.'); ?></p>
<?php endif; ?>
        </div>
<?php get_footer(); ?>