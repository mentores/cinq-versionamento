﻿<?php get_header(); ?>

<ul class="nav navbar-nav nav-quali"> 
<?php wp_nav_menu( array('theme_location' => 'menu-quali','menu_class' => 'nav navbar-nav nav-quali', 'menu' => '','container' => 'ul','menu_id' => '',)); ?>
    </ul>
                    
                    
<?php if ( have_posts() && get_post_status() != 'cancelado') : while ( have_posts() ) : the_post(); ?>


    <h1><?php the_title(); ?></h1>  
    <div class="col-md-8">
<?php the_content(); 

$SQL = "SELECT 
		    p.ID,
		    u.display_name,
		    pms.meta_value AS status,
		    pmv.meta_value AS version
		FROM
		    wp_posts p
		        INNER JOIN
		    wp_postmeta pmv ON (p.ID = pmv.post_id)
		        INNER JOIN
		    wp_postmeta pms ON (p.ID = pmv.post_id)
				INNER JOIN
			wp_users u ON p.post_author = u.ID
		WHERE
			p.post_parent = ".get_the_ID()."
				AND pmv.post_id = pms.post_id
				AND p.post_type = 'revision'
				AND pmv.meta_key = 'dq_version'
		        AND pmv.meta_value != ''
		        AND pms.meta_key = 'dq_status'
		        AND pms.meta_value != ''
		ORDER BY version DESC
		LIMIT 4";
$result = $wpdb->get_results($SQL);
?>
<hr>
	<table style="width:100%">
  <tr>
    <th>Elaborador</th>
    <th>Verificado</th> 
    <th>Aprovador</th> 
    <th>Versão</th> 
  </tr>
   <tr>
    <td><?php echo $result[3]->display_name; ?></td>
    <td><?php echo $result[2]->display_name; ?></td> 
    <td><?php echo $result[1]->display_name; ?></td> 
    <td><?php echo $result[0]->version; 	?></td> 
  </tr>
</table>


<?php endwhile; else: ?>
    <p><?php _e('Sorry, this page does not exist.'); ?></p>
<?php endif; ?>
        </div>
<?php

get_footer(); 