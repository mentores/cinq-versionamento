<?php if(isset($_SESSION) && count($_SESSION)){
		
	$dia_semana = date("W") - date("W", strtotime(date("Y-m-01", time())))+2;

	$data_curr = [];
	$data_curr['id_colaborador'] = $_SESSION['WPOA']['ID_COLABORADOR'];
	$data_curr['mes'] = $dia_semana === 1 ? date("m")-1 : date("m");
	// $data_curr['mes'] = 08;
	$data_curr['ano'] = $dia_semana === 1 && date("m") === 1 ? date("Y")-1 : date("Y");
	switch (TRUE) {
        case 1  == $data_curr['mes']:    $mes = "Janeiro";     break;
        case 2  == $data_curr['mes']:    $mes = "Fevereiro";   break;
        case 3  == $data_curr['mes']:    $mes = "Março";       break;
        case 4  == $data_curr['mes']:    $mes = "Abril";       break;
        case 5  == $data_curr['mes']:    $mes = "Maio";        break;
        case 6  == $data_curr['mes']:    $mes = "Junho";       break;
        case 7  == $data_curr['mes']:    $mes = "Julho";       break;
        case 8  == $data_curr['mes']:    $mes = "Agosto";      break;
        case 9  == $data_curr['mes']:    $mes = "Setembro";    break;
        case 10 == $data_curr['mes']:    $mes = "Outubro";     break;
        case 11 == $data_curr['mes']:    $mes = "Novembro";    break;
        case 12 == $data_curr['mes']:    $mes = "Dezembro";    break; 
 }
	$data_curr['mes_desc'] = substr($mes,0,3);
	
	
	

	// $data_curr['id_colaborador'] = 1721;
	 
    $token    = get_user_meta( wp_get_current_user()->ID, 'token_access', true );

    $url_api  = rtrim(get_option('wpoa_api_url'), '/');



    ########################################
    # COLABORADOR SALDO
    $url_param = $url_api."/ColaboradorExtrato/GetSaldoAcumuladoColaborador?mes=".$data_curr['mes']."&ano=".$data_curr['ano']."&idColaborador=".$data_curr['id_colaborador'];
    $response = wp_remote_request(  $url_param , 
                 [  'method'   => 'GET',
                    'blocking' => true,
                    'headers'  => ['Authorization' => "Bearer $token"]] );
    
    $saldo = json_decode($response['body']);
    ########################################
    # RANKING
    $url_param = $url_api."/ColaboradorExtrato/GetRankingMensal?mes=".$data_curr['mes']."&ano=".$data_curr['ano'];
    $response = wp_remote_request(  $url_param , 
                 [  'method'   => 'GET',
                    'blocking' => true,
                    'headers'  => ['Authorization' => "Bearer $token"]] );
    
    $ranking = json_decode($response['body']);
    ########################################
    

  ?>

  <!---- LAYOUT DO WIDGET---->
  <div class="cinq-points">
    <div class="row">
      <div class="col-lg-4 col-md-12">
      	<h3 class="points-title">Meus pontos</h3>
      </div>
      <div class="col-lg-4 col-md-12">
        <div class="points-mes">
          <div class="points"><?php echo $saldo;?></div>
          <div class="mes-ano"><?php echo $data_curr['mes_desc'],"/",$data_curr['ano'];?></div>
        </div>
      </div>
      <div class="col-lg-4 col-md-12 points-hist">
        <a href="">Ver histórico completo</a>
      </div>
    </div>
      
     <div class="row"> 
<div class="col-lg-5 col-md-12">
      <h4>Ranking mensal</h4></div>
	<div class="col-lg-7 col-md-12 text-right" style="padding-top: 10px;">
		Data de fechamento: 05/11/1984
	  </div>
    </div>
      <ul>
          <?php foreach ($ranking as $data) {?>
	          	<li>
	          		<i class="fa fa-trophy rank<?php echo $data->posicao; ?>" aria-hidden="true"></i> 
	          		<?php echo $data->nomeColaborador;?> <span class="alignright"><?php echo $data->pontos;?></span>
	          	</li>
	        <?php } 
          if(!count($ranking)){?>
            <li>
              <i class="fa fa-trophy rank" aria-hidden="true"></i> 
               Nenhum colaborador no ranking ainda!
            </li>

          <?php } ?>
       </ul>
       
       <p class="text-right">
          <a href="">Ver ranking completo</a>
       </p>
                
  	</div>
    
    <!---- ---->
<?php } ?>
