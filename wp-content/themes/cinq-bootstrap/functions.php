<?php 

add_action('get_header', 'remove_admin_login_header');
function remove_admin_login_header() {
	remove_action('wp_head', '_admin_bar_bump_cb');
}

function wpbootstrap_scripts_with_jquery()
{
	// Register the script like this for a theme:
	wp_register_script( 'custom-script', get_template_directory_uri() . '/js/bootstrap.js', array( 'jquery' ) );
	// For either a plugin or a theme, you can then enqueue the script:
        wp_enqueue_script( 'custom-script' );
	wp_register_script('navegador', get_template_directory_uri() . '/js/Identificar_Navegador.js', array( 'jquery' ) );
	    wp_enqueue_script( 'navegador' );
    wp_register_script('navegador2', get_template_directory_uri() . '/js/navegador.js', array( 'jquery' ));
        wp_enqueue_script( 'navegador2' );
    
    wp_register_script('cookiejs', get_template_directory_uri() . '/js/jquery.cookie.js', array( 'jquery' ), "1.0.0.1", $in_footer = false);
        wp_enqueue_script( 'cookiejs' );
    wp_register_script('menujs', get_template_directory_uri() . '/js/menu.js', array( 'jquery' ), "1.0.0.1", $in_footer = false);
        wp_enqueue_script( 'menujs' );
		//wp_register_script('data-mask', get_template_directory_uri() . '/js/data-mask.js', array( 'jquery' ));
        //wp_enqueue_script( 'data-mask' );

}
add_action( 'wp_enqueue_scripts', 'wpbootstrap_scripts_with_jquery' );

function pull_masonry()
{
    wp_enqueue_script( 'masonry' );
}
add_action( 'wp_enqueue_scripts', 'pull_masonry' );

function pull_mask()
{
    wp_register_script('inputmaskjs', get_template_directory_uri() . '/js/inputmask.js', array( 'jquery' ), '1', false);
    wp_enqueue_script( 'inputmaskjs' );

      wp_register_script('jinputmaskjs', get_template_directory_uri() . '/js/jquery.inputmask.js', array( 'jquery' ), '1', false);
    wp_enqueue_script( 'jinputmaskjs' );
    
    wp_register_script('mask-extensions', get_template_directory_uri() . '/js/inputmask.phone.extensions.js', array( 'jquery' ), '1', false);
    wp_enqueue_script( 'mask-extensions' );

    wp_register_script('maskjs', get_template_directory_uri() . '/js/mask.js', array( 'jquery' ), '1', false);
    wp_enqueue_script( 'maskjs' );

    wp_register_script('scriptjs', get_template_directory_uri() . '/js/script.js', array( 'jquery' ), '1', false);
    wp_enqueue_script( 'scriptjs' );
}
add_action( 'wp_enqueue_scripts', 'pull_mask' );


function pure_js(){
    // JS NO FINAL PARA NAUM BLOQUEAR A RENDERIZACAO
    wp_enqueue_script(  'pure_js' , get_template_directory_uri() . '/js/crossover_pm.js', [], 
                        "1.0.0.1"   , $in_footer = true );
}
add_action( 'wp_enqueue_scripts', 'pure_js' );

function my_scripts_method() {
	wp_enqueue_script('navegador', get_template_directory_uri() . '/js/Identificar_Navegador.js', array( 'jquery' ) );
	wp_enqueue_script( 'navegador' );
}
add_action( 'wp_enqueue_scripts', 'my_scripts_method' );

function kk_in_widget_form($t,$return,$instance){
    $instance = wp_parse_args( (array) $instance, array( 'title' => '', 'text' => '', 'float' => 'none') );
    if ( !isset($instance['col']) )
        $instance['col'] = null;
    if ( !isset($instance['panel']) )
        $instance['panel'] = null;
    ?>
    <p>
        <label for="<?php echo $t->get_field_id('col'); ?>">Tamanho:</label>
        <select id="<?php echo $t->get_field_id('col'); ?>" name="<?php echo $t->get_field_name('col'); ?>">
            <option <?php selected($instance['col'], '8');?> value="8">8</option>
            <option <?php selected($instance['col'], '4');?>value="4">4</option>sho
        </select>
    </p>

  <p>
        <label for="<?php echo $t->get_field_id('panel'); ?>">Cor:</label>
        <select id="<?php echo $t->get_field_id('panel'); ?>" name="<?php echo $t->get_field_name('panel'); ?>">
            <option <?php selected($instance['panel'], 'blue');?> value="blue">blue</option>
            <option <?php selected($instance['panel'], 'green');?>value="green">green</option>
            <option <?php selected($instance['panel'], 'red');?>value="red">red</option>
            <option <?php selected($instance['panel'], 'grey');?>value="grey">grey</option>
            <option <?php selected($instance['panel'], 'yellow');?>value="yellow">yellow</option>
            <option <?php selected($instance['panel'], 'purple');?>value="purple">purple</option>
            <option <?php selected($instance['panel'], 'dblue');?>value="dblue">dark blue</option>
        </select>
    </p>
    <?php
    $retrun = null;
    return array($t,$return,$instance);
}

function kk_in_widget_form_update($instance, $new_instance, $old_instance){
    $instance['col'] = $new_instance['col'];
    $instance['panel'] = $new_instance['panel'];
    return $instance;
}

function kk_dynamic_sidebar_params($params){
    global $wp_registered_widgets;
    $widget_id = $params[0]['widget_id'];
    $widget_obj = $wp_registered_widgets[$widget_id];
    $widget_opt = get_option($widget_obj['callback'][0]->option_name);
    $widget_num = $widget_obj['params'][0]['number'];
    
            if(isset($widget_opt[$widget_num]['col']))
                    $col = $widget_opt[$widget_num]['col'];
            else
                $col = '';
            $params[0]['before_widget'] = preg_replace('/class="grid col-sm-12 col-md-/', 'class="grid col-sm-12 col-md-'.$col,  $params[0]['before_widget'], 1);

             if(isset($widget_opt[$widget_num]['panel']))
                    $panel = $widget_opt[$widget_num]['panel'];
            else
                $panel = '';
            $params[0]['before_widget'] = preg_replace('/class="panel panel-/', 'class="panel panel-'.$panel,  $params[0]['before_widget'], 1);
    
    return $params;
}

//Add input fields(priority 5, 3 parameters)
add_action('in_widget_form', 'kk_in_widget_form',5,3);
//Callback function for options update (priorität 5, 3 parameters)
add_filter('widget_update_callback', 'kk_in_widget_form_update',5,3);
//add class names (default priority, one parameter)
add_filter('dynamic_sidebar_params', 'kk_dynamic_sidebar_params');




if ( function_exists('register_sidebar') )
	register_sidebar(array(
		'name'          => __( 'Painel do Colaborador' ),
		'id'            => 'painel-colaborador',
		'before_widget' => '<div class="grid col-sm-12 col-md-"><div class="panel panel-">',
		'after_widget' => '</div></div>',
		'before_title' => '<div class="panel-heading"><h3 class="panel-title">',
		'after_title' => '</h3></div>',

	));
	
	function register_my_menus() {
  register_nav_menus(
    array(
		'menu-lateral' => __ ( 'Menu Lateral'), 
      'menu-usuario' => __( 'Menu de Usuário' ),
      'menu-rh' => __( 'Documentos do RH' ),
      'menu-quali' => __( 'Documentos da Qualidade' ),
      'menu-solicitacoes' => __( 'Solicitações' ),
    )
  );
}
add_action( 'init', 'register_my_menus' );


// Removes ul class from wp_nav_menu
function remove_ul ( $menu ){
    return preg_replace( array( '#^<ul[^>]*>#', '#</ul>$#' ), '', $menu );
}
add_filter( 'wp_nav_menu', 'remove_ul' );


/*Shortcodes*/
add_filter( 'widget_title', 'do_shortcode' );

add_filter('wp_nav_menu_items', 'do_shortcode');
 
add_shortcode( 'fa', 'so_shortcode_fa' );
function so_shortcode_fa( $attr, $content ) {
    return '<i class="fa fa-'. $content . '"></i>';
}


add_shortcode( 'di', 'so_shortcode_di' );
function so_shortcode_di( $attr, $content ) {
    return '<i class="dashicons dashicons-'. $content . '"></i>';
}


add_shortcode( 'mi', 'so_shortcode_mi' );
function so_shortcode_mi( $attr, $content ) {
    $menutoggle = isset($_COOKIE['nav-toggle'])?!(bool)$_COOKIE['nav-toggle']:false;
    return '<span class="menu-item-text '.($menutoggle? 'menu-item-text-off' :'').'">'. $content . '</span>';
}

add_shortcode( 'ci', 'so_shortcode_ci' );
function so_shortcode_ci( $attr, $content ) {
    return '<span class="cinqicon cinqicons-'. $content .'">' . '</span>';
}



add_filter('widget_text', 'do_shortcode');

add_shortcode('ideas','ideas_shortcode');
function ideas_shortcode(){
	ob_start();
	
	$args = array(
		'post_type' => 'ideas',
		'posts_per_page' => -1
	);
	
	$q = new WP_Query($args);
	
	if($q->have_posts()) : while($q->have_posts()) : $q->the_post();
    ?>
		<li><a href="<?php echo the_permalink(); ?>"> <?php echo the_title(); ?> </a></li>

<?php
	endwhile;endif;
	
	
	return ob_get_clean();
}


/*function new_excerpt_more($more) {
       global $post;
	return ' [...]<div class="readmore alignright"> <a class="moretag" href="'. get_permalink($post->ID) . '"> Leia mais <i class="fa fa-arrow-circle-right"></i></a></div>';
}
add_filter('excerpt_more', 'new_excerpt_more');


function custom_excerpt_length( $length ) {
    return 18;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

*/

function remove_more_link_scroll( $link ) {
	$link = preg_replace( '|#more-[0-9]+|', '', $link );
	return $link;
}
add_filter( 'the_content_more_link', 'remove_more_link_scroll' );


add_theme_support( 'post-thumbnails' ); 

if(is_admin()){
add_filter( 'override_post_lock', create_function("", 'return false;' ));
}
