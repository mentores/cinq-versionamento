<? /*Template Name: RH */?>

<?php get_header(); ?>

<ul class="nav navbar-nav nav-quali"> 
<?php wp_nav_menu( array('theme_location' => 'menu-rh','menu_class' => 'nav navbar-nav nav-quali', 'menu' => '','container' => 'ul','menu_id' => '',)); ?>
    </ul>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>



	

<h1><?php the_title(); ?></h1>	
<div class="col-md-8">
<?php the_content(); ?>
    </div>
<?php endwhile; else: ?>
	<p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
<?php endif; ?>

<?php get_footer(); ?>
