<? /*Template Name: Qualidade */?>

<?php get_header(); ?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>



	
  <ul class="nav navbar-nav nav-quali"> 
<?php wp_nav_menu( array('theme_location' => 'menu-quali','menu_class' => 'nav navbar-nav nav-quali', 'menu' => '','container' => 'ul','menu_id' => '',)); ?>
    </ul>

<h1><?php the_title(); ?></h1>	
<?php the_content(); ?>

<?php endwhile; else: ?>
	<p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
<?php endif; ?>

<?php get_footer(); ?>
