<? /*Template Name: Solicitações. */?>

<?php get_header('iframe'); ?>

<!--panel panel-info panel-round-->
<div class="solicitacoes-wrapper">

	<div class="container-fluid" style="height:75px">
		<div class="menu-arrow menu-left-arrow" >
			<i class="fa fa-chevron-left"></i>
		</div>
		
		<ul class="nav navbar-nav top-nav menu-ul">
			<?php wp_nav_menu(array(
							'theme_location' 	=> 'menu-solicitacoes',
							'menu_class' 		=> 'nav navbar-nav nav-solicitacoes',
							'menu' 				=> '',
							'container'			=> 'ul',
							'menu_id' 			=> '',)); ?>
		</ul>

		<div class="menu-arrow menu-right-arrow" style="">
			<i class="fa fa-chevron-right"></i>
		</div>
	</div>

	<div class="solicitacoes-container">

		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

			<?php the_content(); ?>

		<?php endwhile; else: ?>
			<p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
		<?php endif; ?>

		<?php get_footer(); ?>
	</div>
</div>