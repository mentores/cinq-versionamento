function Crossover() {
    var self 	= this;
    this.listeners 		= [];
    this.parent 		= (window.location == window.parent.location ? true : false);

    window.addEventListener('crossover', function(e){
		self.listen(e.detail);
	});
	if(!this.parent)
	    this.handshake('parent', function(){});

} Crossover.prototype = {
	handshake : function(element,data,param){
		var self  = this;
		this.listeners[element] = { on 	   : data.toString()	,
									param  : param || {}		,
									origin : "" 				,
									load   : false 				}
		
		if(this.parent){
			(document.getElementById(element)).addEventListener("load", function() { // ON LOAD CHILD
				self.listeners[element].origin = "parent";
				self.send( element, 'handshake>');
			});
		}

		return this;	
	},
	listen  : function(e){
		switch(e.action) {
		    case 'handshake>': // ON CHILD

		    	this.listeners['parent'].origin = e.element;
				this.send( 'parent' , 'handshake<' );
				e.on 	=  eval('(' + e.on + ')');
		    	e.on.call(window,this, e, e.param );

				break;
		    case 'handshake<': // ON MASTER
		    case 'return':
		    	e.on =  eval('(' + e.on + ')');
			    e.on.call(window,this, e, e.param  );
		    	break;
		}
    },
    on 		: function(el,data,param){
    	this.return(el,data, param || {});
	},
    return 	: function(el,data,param){
    	this.listeners[el].param = param || {};
    	this.listeners[el].on 	 = data.toString();
    	this.send(el,'return');
	},
    send 	: function(el,action){
    	var event = document.createEvent('CustomEvent');
		event.initCustomEvent("crossover", true, true, {'action'	: action 					, 
									    			 	'element' 	: el 						,
									    			 	'on' 		: this.listeners[el].on		, 
									    			 	'param' 	: this.listeners[el].param 	,
									    			 	'origin' 	: this.listeners[el].origin	});
		if(el == "parent")
			window.parent.document.dispatchEvent(event);
		else
			document.all[el].contentDocument.dispatchEvent(event);
		
    }
}