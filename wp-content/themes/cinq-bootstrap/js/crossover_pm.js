function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

/** constructor. */
function Crossover() {
    var self 	        = this;
    this.listeners 		= [];
    this.handshakeON	= false;
    this.parent 		= (window.location == window.parent.location ? true : false);
	
    window.addEventListener('message', function(e){
		window.crossover.listen(e);
	});

	if(!self.parent)
	    self.handshake('parent', function(){});
} 

/** add functions here */
Crossover.prototype = {
	
	/** crossover.handshake(receiving_element_id, function, [(opt) function_parameter])
	*	
	*	parameters:
	*		receving_element_id		inner element to react to receiver's response
	*		function				function implementing receiver's response to this handshake
	*		function_parameter		parameter to sent function
	*/
	handshake : function(element,data,param)
	{
		var self  = this;
		

		this.listeners[element] = { on 	   	: data.toString()	,
									param 	: param || {}		,
									origin 	: "" 				,
									receiver: {} 				};
		
		if(this.parent){
			var el 	  						 = document.getElementById(element);
			this.listeners[element].receiver =  el.contentWindow || el.contentDocument;
			this.listeners[element].origin 	 = "parent";
			
			(document.getElementById(element)).addEventListener("load", function() { // ON LOAD CHILD
				self.send( element, 'handshake>');
			});
		} else {
			self.listeners[element].receiver = parent;
		}
		
		return this;	
	},
	
	/** listener thread */
	listen  : function(event)
	{
		var e = event.data;
		
		switch(e.action) {
			// child sends handshake with action for master
		    case 'handshake>': // ON CHILD
		    	this.listeners['parent'].origin = e.element;
				this.send( 'parent' , 'handshake<' );

				e.on 	=  eval('(' + e.on + ')');
		    	e.on.call(window,this, e, e.param );
				break;
			
			// master replies handshake with action for child
		    case 'handshake<': // ON MASTER
		    	e.on =  eval('(' + e.on + ')');
			    e.on.call(window,this, e, e.param  );
				
				this.send( e.origin , 'handshake>>');			    
			    break;
			
			// after master sends reply, confirm and start data exchange
			case 'handshake>>': // ON CHILD
		    	this.handshakeON = true;	
		    	this.send( e.origin , 'handshake<<');	    
			    break;
			
			// receives confirmationm start data exchange
			case 'handshake<<': // ON MASTER
		    	this.handshakeON = true;	    
			    break;
			
			// ask for return of function sent
			case 'return':
		    	e.on =  eval('(' + e.on + ')');
			    e.on.call(window,this, e, e.param  );
		    	break;
		}
    },
    
	/** crossover.on(requester, function, [(opt) function_parameter])
	*	
	*	receives any functions from determined sender.
	*
	*	parameters:
	*		requester			website that sent request
	*		function			function to be executed
	*		function_parameter	(optional) function parameter
	*/
	on 		: function(el,data,param)
	{
		if(typeof(this.listeners[el]) == 'undefined'){
			console.log("erro: unknown element '"+el+"'");
		}else{
	    	var self = this;
	    	var intervalHandshake = setInterval(function(){
	    		if(self.handshakeON){
	    			self.return(el,data, param || {});	
	    			clearInterval(intervalHandshake);
	    		}
	    	},100);
	    }
    },
    
	/** crossover.return(requester, function, [(opt) function_parameter])
	*	
	*	returns to requester a function.
	*
	*	parameters:
	*		requester			website requesting the called function (access by data.origin from nested over function)
	*		function			reply function
	*		function_parameter	optional function parameter
	*/
	return 	: function(el,data,param){
		if(typeof(this.listeners[el]) == 'undefined'){
			console.log("erro: unknown element '"+el+"'");
		}else{
	    	this.listeners[el].param = param || {};
	    	this.listeners[el].on 	 = data.toString();
	    	this.send(el,'return');
	    }
	},
	
	/** crossover.send(receiver, action_string)
	*
	*	send to receiver a message defined in action.
	*	possible actions:
	*		'action'		action to analyze in thread
	*		'on'			function
	*		'param'			parameters
	*		'element'		receiving_element_id
	*		'origin'		sender id
	*		anything else	message
	*/
    send 	: function(el,action){
    	this.listeners[el].receiver.postMessage( {	'action'	: action 					, 
								    			 	'on' 		: this.listeners[el].on		, 
								    			 	'param' 	: this.listeners[el].param 	,
								    			 	'element' 	: el 						,
								    			 	'origin' 	: this.listeners[el].origin}, 
								    			 	'*' );
	}
}

window.crossover = new Crossover();


