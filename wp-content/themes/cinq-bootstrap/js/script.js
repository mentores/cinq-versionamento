jQuery(document).ready(function(){

	jQuery('.radio-language').change(function function_name() {
		
		if(jQuery(this).is(':checked')){
				
				if(jQuery(this).val()=='portugues'){
					jQuery('.setor-portugues').attr('id','setor');
					jQuery('.setor-portugues').attr('name','setor');
					jQuery('.setor-ingles').attr('id','setor2');
					jQuery('.setor-ingles').attr('name','setor2');
				}else{
					jQuery('.setor-ingles').attr('id','setor');
					jQuery('.setor-ingles').attr('name','setor');
					jQuery('.setor-portugues').attr('id','setor2');
					jQuery('.setor-portugues').attr('name','setor2');
				}
				jQuery('#setor').show();
				jQuery('#setor2').hide();
		}
	})

});