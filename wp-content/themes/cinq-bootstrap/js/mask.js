jQuery(document).ready(function(){

  function applyMask(){
      jQuery('#celular').inputmask(
              {
                  mask: '(99) 9999[9]-9999',
                  clearMaskOnLostFocus: true,
                  skipOptionalPartCharacter: "-",
                  "onincomplete": function () {
                      formateFone(this);
                  }
              });
        jQuery('#ramal').inputmask(
              {
                  mask: '(99) 9999[9]-9999',
                  clearMaskOnLostFocus: true,
                  skipOptionalPartCharacter: "-",
                  "onincomplete": function () {
                      formateFone(this);
                  }
              });

  }
  function formateFone($this) {
          var formatedFone = $this.value.substring(0, 9) + '-' + $this.value.substring(9, 14).replace('-', '');
          jQuery($this).val(formatedFone);
  }
  
  applyMask();


});

function validarCamposTelefone(){

    var campoCelular = jQuery('#celular').val();
    var campoTelefone = jQuery('#ramal').val();

     var mask = /\(\d\d\)\s\d\d\d\d-\d\d\d\d/;
     var mask2 = /\(\d\d\)\s\d\d\d\d\d-\d\d\d\d/;
     var match = campoCelular.match(/\(__\)\s\____-____/) != null;

 
     var matchCelular = campoCelular.match(mask);
     var matchTelefone = campoTelefone.match(mask);

     var matchCelular2 = campoCelular.match(mask2);
    
     var celularCorrect = campoCelular.length == 0 || match|| ((matchCelular!= null && campoCelular.length >= 14 && matchCelular.length == 1)  || (matchCelular2!= null && campoCelular.length >= 14 && matchCelular2.length == 1));

     var telefoneCorrect = (matchTelefone!= null && campoTelefone.length >= 14 && matchTelefone.length == 1);

     if(!celularCorrect) jQuery('#celular').css("border-color", "#ff0000");

     if(!telefoneCorrect) jQuery('#ramal').css("border-color", "#ff0000");
    
    return celularCorrect && telefoneCorrect;
}
function copiarAssinatura(){
  selectElementContents(jQuery('#tabelaAssinatura')[0]);
  document.execCommand("copy");
  window.getSelection().removeAllRanges();
  alert('Assinatura copiada');
 return false;
}

function selectElementContents(el) {
    var body = document.body, range, sel;
    if (document.createRange && window.getSelection) {
        range = document.createRange();
        sel = window.getSelection();
        sel.removeAllRanges();
        try {
            range.selectNodeContents(el);
            sel.addRange(range);
        } catch (e) {
            range.selectNode(el);
            sel.addRange(range);
        }
    } else if (body.createTextRange) {
        range = body.createTextRange();
        range.moveToElementText(el);
        range.select();
    }
}