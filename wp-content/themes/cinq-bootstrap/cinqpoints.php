
  <!---- LAYOUT DO WIDGET---->
  <div class="cinq-points">
    <div class="row">
      <div class="col-sm-4 text-right" style="padding: 0px;">
      	<h3 class="points-title">Meus pontos</h3>
      </div>
      <div class="col-sm-4">
        <div class="points-mes">
          <div class="points">1300</div>
          <div class="mes-ano">AGO/2016</div>
        </div>
      </div>
      <div class="col-sm-4 points-hist" style="padding: 0px;">
        <a href="">Ver histórico completo</a>
      </div>
    </div>
  
    <h4>Ranking mensal</h4>
       <ul>
          <li><i class="fa fa-trophy rank1" aria-hidden="true"></i> Daniel Bastos <span class="alignright">500</span></li>
          <li><i class="fa fa-trophy rank2" aria-hidden="true"></i> Helena Maura <span class="alignright">400</span></li>
          <li><i class="fa fa-trophy rank3" aria-hidden="true"></i> Leonardo de Souza <span class="alignright">300</span></li>
       </ul>
       
       <p class="text-right">
          <a href="">Ver ranking completo</a>
       </p>
                
  	</div>
    
    <!---- ---->

