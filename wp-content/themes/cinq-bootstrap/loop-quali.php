

<table class="table table-hover">
   <thead> <tr>
        <th>Nome do documento</th>
        <th>Número da Revisão</th>
        <th>Elaborador</th>
    </tr>
       </thead>
    <tbody>


<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	
           <tr>
               <td><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></td>
               <td><?php echo rwmb_meta( 'rw_nrevisao' ); ?></td>
               <td><?php echo rwmb_meta( 'rw_elaborador'); ?></td>
               
           </tr>


<?php endwhile; else: ?>
	<p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
<?php endif; ?>
</tbody>
</table>
