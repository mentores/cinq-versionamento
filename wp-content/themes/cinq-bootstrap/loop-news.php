<ul class="news">

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>


	<li>

<div class="post-thumbnail"> <a href="<?php echo get_permalink();?>"><?php if ( has_post_thumbnail() ) {
    the_post_thumbnail('thumbnail');
} ?> </a></div>

<h4> <?php the_title(); ?></h4>	
	 <?php the_content(' [...]<div class="readmore alignright"> <a class="moretag" href="'. get_permalink($post->ID) . '"> Leia mais <i class="fa fa-arrow-circle-right"></i></a></div>'); ?>
<i><?php the_time('d/m/y');?> </i>
        </li>
       
<?php endwhile; else: ?>
    
    
	<p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
<?php endif; ?>

<p class="text-right"><a href="noticias" > Ver todas as notícias</a> </p></ul>