<?php
/**
 * SCRIPT CUSTOM
 */
function customScript() {
	
	wp_enqueue_script('disable_visibility_script',	   WPMU_PLUGIN_URL.'/workflow/js/disable_visibility.js', false, '1.0.0');
	wp_enqueue_script('global_script',				   WPMU_PLUGIN_URL.'/workflow/js/global.js', false, '1.0.0');
	
	wp_enqueue_style('hidden_visibility_date_publish', WPMU_PLUGIN_URL.'/workflow/css/hidden_visibility_date_publish.css', false, '1.0.0');
}
if (is_documents()) :
	add_action( 'admin_enqueue_scripts', 'customScript');
endif;
if (isset($_GET['post']) && get_post_type($_GET['post']) == 'versao') :
	function customScriptVersion() {
		wp_enqueue_script('global_version_js',			   WPMU_PLUGIN_URL.'/workflow/js/globalVersion.js', false, '1.0.0');
		wp_enqueue_style('hidden_visibility_date_publish', WPMU_PLUGIN_URL.'/workflow/css/hidden_visibility_date_publish.css', false, '1.0.0');
		wp_enqueue_style('global_version',				   WPMU_PLUGIN_URL.'/workflow/css/globalVersion.css', false, '1.0.0');
	}
	add_action( 'admin_enqueue_scripts', 'customScriptVersion');
endif;