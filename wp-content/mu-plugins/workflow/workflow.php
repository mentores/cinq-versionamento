<?php
/*
* Plugin name: CINQ WorkFlow
* Description: Controle de documentos da CINQ.
* Version: 1.0
* Author: Mentores
*/
 
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

	
require dirname(__FILE__).'/utils.php';
require dirname(__FILE__).'/config.php';
require dirname(__FILE__).'/register-post-type.php';
require dirname(__FILE__).'/register-taxonomy.php';
require dirname(__FILE__).'/custom-script-front-end.php';
require dirname(__FILE__).'/classDocument.php';

if ( ! is_documents()) return false;

require dirname(__FILE__).'/globalFunction.php';