<?php
global $orderStatus;

function globalCss() {    
    wp_enqueue_style('global_css', WPMU_PLUGIN_URL.'/workflow/css/global_admin.css', false, '1.0.0');
}
add_action('admin_enqueue_scripts', 'globalCss');

/**
 * DEFINE
 */
define('DIR_WORKFLOW', '/wp-content/mu-plugins/workflow/');
$orderStatus = array(
    array(
        'name'          => 'Em elaboração',
        'textAction'    => 'Finalizar elaboração'
    ),
    array(
        'name'          => 'Elaborado',
        'textAction'    => 'Enviar para aprovação',
        'validFunction' => 'sendToapproveDocument()'
    ),
    array(
        'name'          => 'Verificado',
        'textAction'    => 'end'
    ),
);
define('FIRST_STATUS', $orderStatus[0]['name']);