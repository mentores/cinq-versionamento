<?php

function removeOldVersion($query) {

  if ($query->query['attachment'] != 'edit.php' && ! $query->is_main_query()) return false;

    $query->set('tax_query', array(
        array(
            'taxonomy' => 'document_status',
            'field'    => 'name',
      			'terms'    => 'Elaborando nova versão',
            'operator' => 'NOT IN'
        )
    ));
    
}
add_action('pre_get_posts','removeOldVersion');

function wpse_add_my_view($views)
{
  
  global $post_type_object;
  
  unset($views['publish']);
  
  
  $allStatus = array(
		'Aprovado',
		'Cancelado',
		'Elaborado',
		'Em elaboração',
		'Reprovado',
		'Verificado'
	);
	$total = 0;
	foreach($allStatus as $status) :
    $viewStatus = new WP_Query(array( 
        'post_type'      => 'documentos',
        'posts_per_page' => -1,
        'tax_query' => array(
      		array(
      			'taxonomy' => 'document_status',
      			'field'    => 'name',
      			'terms'    => $status,
      		),
      		array(
      			'taxonomy' => 'document_status',
      			'field'    => 'name',
      			'terms'    => 'Elaborando nova versão',
      			'operator' => 'NOT IN'
      		),
      	)
    ));
    $total += $viewStatus->post_count;
    $views[$status] = "<a href='".admin_url()."edit.php?post_type=documentos&document_status=".sanitize_title($status)."'>".$status."</a> (".$viewStatus->post_count.")";
  endforeach;
  
  $views['all'] = "<a href='".admin_url()."edit.php?post_type=documentos&all_posts=1'>Todos</a> (".$total.")";

  return $views;
}
add_filter( 'views_edit-documentos', 'wpse_add_my_view');
