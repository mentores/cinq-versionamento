(function($){
    try {
    var idDocument = location.hash.replace('#document=', '');
    
    $('#title,#content').attr('disabled', 'disabled');
    $('#wpbody-content h1').html('<a href="/wp-admin/admin.php?page=versoes-documento&idDocument='+idDocument+'" class="page-title-action">Voltar</a>').show();
    
    } catch(err) {
        console.log(err);
    }
}) (jQuery);
