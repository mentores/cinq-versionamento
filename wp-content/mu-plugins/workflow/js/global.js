(function($){
    try {
        
        $('#submitdiv > h2').html('Documento').show();
        
    } catch(err) {
        console.log(err);
    }
}) (jQuery);

function setNextStatus(newVal)
{
    jQuery('#idNextStatus').val(newVal);
}

function submitPost()
{
    setNextStatus(jQuery('#idNextStatus').attr('data-value'));
    jQuery('#post').submit();
}

function sendToapproveDocument(objt)
{   
    if ( ! validObs()) return false;
    
    setNextStatus(jQuery('#idNextStatus').attr('data-value'));
    jQuery('#post').submit();
}


function approveDocument(objt)
{   
    if ( ! validObs()) return false;
    
    setNextStatus(jQuery(objt).attr('data-tradeStatus'));
    jQuery('#post').submit();
}

function disapproveDocument(objt)
{
    if ( ! validObs()) return false;
    
    setNextStatus(jQuery(objt).attr('data-tradeStatus'));
    jQuery('#post').submit();
}

function cancelToDocument(objt)
{
    setNextStatus(jQuery(objt).attr('data-tradeStatus'));
    jQuery('#post').submit();
}

function repeatEditFlowDocument(objt)
{
    setNextStatus(jQuery(objt).attr('data-tradeStatus'));
    jQuery('#newFlow').val(1);
    jQuery('#post').submit();
}
function validObs()
{
    var obs = jQuery('#documentObs').val();
        obs = obs.replace(/\s/g, '');
        
    if (obs != '')
        return true;
    
    jQuery('#documentObs').css('border','1px solid red');
    jQuery('#requiredObs').show();
    
    
    return false;
}