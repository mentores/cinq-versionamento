<?php
/**
 * Criando os post types
 */
function create_post_type() {
    
    register_post_type('documentos',
      array(
        'labels'          => array(
          'name'          => __( 'Documentos da qualidade' ),
          'singular_name' => __( 'Documentos da qualidade' )
        ),
        'public'      => true,
        'has_archive' => true,
      )
    );
    
    register_post_type('versao',
      array(
        'labels'          => array(
            'name'          => __('Versões'),
            'singular_name' => __('Versão')
        ),
        'public'       => true,
        'has_archive'  => false,
        'show_in_menu' => false,
      )
    );
    
    add_post_type_support( 'documentos', 'custom-fields' );
    add_post_type_support( 'versao', 'custom-fields' );
    
}

add_action('init', 'create_post_type');

add_filter('manage_documentos_posts_columns', 'colunaDocumentos', 10);
add_action('manage_documentos_posts_custom_column', 'conteudoColunaDocumentos', 10, 2);
  
function colunaDocumentos($defaults) {
  $defaults['historic'] = 'Histórico';
  return $defaults;
}
function conteudoColunaDocumentos($column_name, $post_ID) {
  if ($column_name == 'historic') {
      echo '<a href="'.admin_url('/?page=versoes-documento&idDocument='.$post_ID, 'https' ).'" target="_blank">Ver Histórico</a>';
  }
}

/*
* Metodo para registrar novas paginas no admin
*/
function registerPages()
{
    add_menu_page("Ver Versões", "Ver Versões", 'edit_pages', 'versoes-documento', 'pageVersion', '', 81);
}

/**
* Exibe conteúdo da página de versões
*/
function pageVersion()
{
    include_once __DIR__ .'/pages/versoes.php';
}

add_action('admin_menu', 'registerPages');

if ( is_admin() ) :
  
  add_action('load-post.php', 'smashing_add_post_meta_boxes' );
  /* Create one or more meta boxes to be displayed on the post editor screen. */
  function smashing_add_post_meta_boxes() {
  
    add_meta_box(
      'renderHistoyVersion',      // Unique ID
      'Versões',    // Title
      'renderHistoyVersion',   // Callback function
      'documentos',         // Admin page (or post type)
      'normal',         // Context
      'default'         // Priority
    );
  }
  
  /* Display the post meta box. */
  function renderHistoyVersion($post) 
  {
    pageVersion();
  }

endif;