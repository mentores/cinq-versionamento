<?php
global $orderStatus;

class Document
{

	function __construct(){
		
		$GLOBALS['control']   = 0;
		$GLOBALS['controlId'] = 0;
		
		add_action('post_submitbox_start',		  array($this, 'buttonNextStatus'));
		add_action('post_submitbox_misc_actions', array($this, 'buttonActionsTop'));
		
		add_action('publish_documentos',   array($this, 'createVersion'), 10, 2);
		add_action('save_post', 		   array($this, 'newFlow'), 10, 3);
		
		add_filter('gettext',			   array($this, 'textButtonSave'));
	
	}
	
	
	/**
	* Renderizando o campo de observação
	*/
	public function renderTextareaObs()
	{
		echo '<div class="form-field term-description-wrap" style="margin-bottom:10px">';
			echo '<label for="tag-description"><strong>Observação:</strong></label>';
			echo '<div style="
				display:none;
				margin: 5px 0 15px;
				background: #fff;
				border-left: 4px solid #fff;
				-webkit-box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);
				box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);
				padding: 1px 5px;
				border-left-color: #dc3232;" id="requiredObs">
					<p style="padding: 10px 4px;margin: 0px;">Este campo é obrigatório!</p>
				</div>';
			echo '<textarea name="documentObs" id="documentObs" style="width:100%" rows="5" cols="40"></textarea>';
		echo '</div>';
	}
	
	/**
	* Renderizando o botao para aprovar o documento 
	*/
	public function renderButtonApproveDocument()
	{
		
		$approveId = get_term_by('name', 'Aprovado', 'document_status')->term_id;
		
		echo '<input 
				name="toApprove" 
				type="button" 
				class="button button-primary button-large" 
				id="toApprove" 
				value="Aprovar" 
				style="width: 46%; margin-bottom: 20px;margin-left:8%;"
				data-tradeStatus="'.$approveId.'"
				onclick="approveDocument(this)">';
		
	}
	
	/**
	* Renderizando o botao para reprovar o documento 
	*/
	public function renderButtonDisapproveDocument()
	{
		$disapproveId = get_term_by('name', 'Reprovado', 'document_status')->term_id;
		
		echo '<input 
				name="disapprove" 
				type="button" 
				class="button button-primary button-large" 
				id="disapprove" 
				value="Reprovar" 
				style="width: 46%; margin-bottom: 20px;"
				data-tradeStatus="'.$disapproveId.'"
				onclick="disapproveDocument(this)">';
	}

	/**
	* Renderizando o botao para cancelar o documento 
	*/
	public function renderButtonCancelDocument()
	{
		$cancelId = get_term_by('name', 'Cancelado', 'document_status')->term_id;
		
		echo '<div id="delete-action" style="display:block;width: 49%;font-size: 12px;">';
		echo '<a class="submitdelete deletion"
				href="#" 
				onclick="cancelToDocument(this);return false;"
				data-tradeStatus="'.$cancelId.'"
				id="cancelDocument">Cancelar documento</a>';
		// echo '<input 
		// 		name="cancelDocument" 
		// 		type="button" 
		// 		class="submitdelete deletion" 
		// 		id="cancelDocument" 
		// 		value="Cancelar documento" 
		// 		style="width: 100%; margin-bottom: 20px;"
		// 		data-tradeStatus="'.$cancelId.'"
		// 		onclick="cancelToDocument(this)">';
		echo '</div>';
	}
	
	/**
	 * Renderizando botão que colocara o documento em um novo fluxo de edição
	 */
	 public function renderRepeatEditFlow()
	 {
	 	$newFlowId = get_term_by('name', 'Em elaboração', 'document_status')->term_id;
	 	
	 	echo '<input type="hidden" name="newFlow" id="newFlow" value="0">';
	 	
 		echo '<input 
			name="cancelDocument" 
			type="button" 
			class="button button-primary button-large" 
			id="cancelDocument" 
			value="Criar nova versão" 
			style="width: 50%;"
			data-tradeStatus="'.$newFlowId.'"
			onclick="repeatEditFlowDocument(this)">';
	 }
	
	public function setId($id)
	{
		$this->id = $id;
	}
	
	public function getId()
	{
		if (empty($this->id)) return $_GET['post'];
		 
		return $this->id;
	}	
	
	public function setOrderStatus(array $order)
	{
		$this->orderStatus = $order;
	}
	
	public function getOrderStatus()
	{
		return $this->orderStatus;
	}
	
	/**
	 * Metodo para trocar o nome do botão de salvar de acordo com a necessidade
	 */
	public function textButtonSave($text)
	{
		if ( ! is_documents()) return $text;
		
		switch ($text) {
			case 'Publicar':
			case 'Atualizar':
			    $text = 'Salvar';
		}
		return $text;
	}


	/**
	 * Metodo para setar o primeiro status definido no define FIRST_STATUS
	 */ 
	public function setFirstStatus() 
	{
		$firstStatus = get_term_by('name',FIRST_STATUS, 'document_status');
		wp_set_post_terms($this->getId(), $firstStatus->term_id, 'document_status', true);
	}
	
	/**
	 * Metodo para deletar todos os termos do documento
	 */
	public function deleteAllTerms()
	{
		$termsPost = wp_get_post_terms($this->getId(), 'document_status');
		if (null == $termsPost) return false;
		
		foreach ($termsPost as $term) {
			wp_remove_object_terms($this->getId(), $term->term_id, 'document_status');
		}
	}
	
	/**
	 * Metodo que verifica se o documento esta aprovado
	 */
	 public function isApproved()
	 {
	 	return $this->existStatusInDocument('Aprovado');
	 }
	 
	 /**
	 * Metodo que verifica se o documento esta reprovado
	 */
	 public function isDisapproved()
	 {
	 	return $this->existStatusInDocument('Reprovado');
	 }
	 
	 /**
	  * Metodo que verifica se o documento esta cancelado
	  */
	 public function isCanceled()
	 {
	 	return $this->existStatusInDocument('Cancelado');
	 }
	 
	 /**
	  * Metodo que verifica se o documento esta verificado
	  */
	 public function isVerified()
	 {
	 	return $this->existStatusInDocument('Verificado');
	 }
	 
	 /**
	  * Metodo que verifica se o documento esta elaborado
	  */
	 public function isElaborate()
	 {
	 	return $this->existStatusInDocument('Elaborado');
	 }
	 
	/**
	 * Desabilitando o botão de salvar
	 */
	 public function removeSaveButton()
	 {
		echo '<style>#publish{display:none;}</style>';
	 }
	 
	/**
	* Documento reprovado, continuar para o fluxo de validação
	*/
	public function newValidationFlow()
	{
		$idStatus = $this->getStatusById(FIRST_STATUS);
		$this->setStatus($idStatus);
	}
	
	/**
	 * Pegando o id do status via ID
	 */
	 public function getStatusById($nameStatus)
	 {
	 	return get_term_by('name',$nameStatus, 'document_status')->term_id;
	 }
	 
	 
	/**
	* Pegando o numero da versao atual
	*/
	public function getCurrentVersion()
	{
		return get_post_meta($this->getId(), 'version_order_ref', true);
	}
	
	/**
	* Pegando o nome do status atual
	*/
	public function getCurrentStatus()
	{
		return wp_get_post_terms($this->getId(), 'document_status')[0]->name;
	}
	
	/**
	* Pegando o id do proximo status
	*/
	public function getNextStatus()
	{
		$orderStatus = $this->getOrderStatus();
		
		$nextStatus['id']  = get_term_by('name',$orderStatus[0]['name'],'document_status')->term_id;
		
		foreach($orderStatus as $key => $status) :
			
			if ( ! $this->existStatusInDocument($status['name'])) continue;

			$nextStatus['id']			 = get_term_by('name',$orderStatus[$key+1]['name'], 'document_status')->term_id;
			$nextStatus['textAction']	 = $orderStatus[$key]['textAction'];
			$nextStatus['function'] 	 = $orderStatus[$key]['validFunction'];
			
		endforeach;
		
		return $nextStatus;
	}
	
	/**
	 * Pegando a versao Main
	 */
	 public function getMainVersion()
	 {
	 	if (null == get_post_meta($this->getId(), 'mainVersion', true)) return false;
	 	
	 	return get_post_meta($this->getId(), 'mainVersion', true);
	 }

	/**
	 * Metodo para pegar a categoria do documento
	 */
	 public function getCategory($post_id)
	 {
	 	return wp_get_post_terms($post_id, 'document_categoria')[0]->term_id;
	 }
	 
	/**
	* Setar versão principal para documento aprovado
	*/
	public function setMainVersion()
	{	
		$nameStatus = wp_get_post_terms($this->getId(), 'document_status')[0]->name;
		
		if ( ! $this->isApproved()) return false;
		
		if ( ! $this->getMainVersion()) :
			$this->setFirstVersionMain();
		else:
			$versionNumber = $this->getMainVersion();
			$versionNumber = $versionNumber + 1;
			update_post_meta($this->getId(), 'mainVersion', $versionNumber);
		endif;
		
		return true;
	}
	
	/**
	* Setando a primeira versao main
	*/
	public function setFirstVersionMain()
	{
	  update_post_meta($this->getId(), 'mainVersion', 1);
	}
	
	/**
	 * Metodo para salvar o proximo status do post
	 */
	public function setNextStatus($idNextStatus)
	{
		if ($idNextStatus == 0) :
			//Verificando se tem algum status cadastrato no documento
			$termsPost = wp_get_post_terms($this->getId(), 'document_status');
			if ($termsPost) return false;
			
			//Setando o primeiro status do documento
			$this->setFirstStatus();
			
			//Dando o return para parar com o fluxo
			return false;
		endif;
		
		$this->setStatus($idNextStatus);
	}
	
	/**
	 * Setando um status no documento
	 */
	 public function setStatus($idStatus)
	 {
	 	//Deletando todos os status
		$this->deleteAllTerms();
		//Salvando o proximo status
		wp_set_post_terms($this->getId(), $idStatus, 'document_status', true);
	 }
	 
	/**
	* Metodo que verifica se o status passodo exite no documento
	*/
	public function existStatusInDocument($statusName)
	{
		$termsPost = wp_get_post_terms($this->getId(), 'document_status');
		foreach ($termsPost as $term) {
			if ($term->name == $statusName) :
				return true;
			endif;
		}
		return false;
	}
	
	/**
	* Verificando se é para criar uma versao para o status passado
	*/
	 public function IsToCreateVersion($statusName)
	 {
	 	$isValid = array('Elaborado', 'Verificado', 'Aprovado', 'Cancelado');
	 	
	 	if ( ! in_array($statusName, $isValid)) return false;
	 	
	 	return true;
	 }
	/**
	 * Metodo para redenrizar os botões de acões na parte de cima 
	 */
	 public function buttonActionsTop()
	 { 
	 	global $post;
		if ($post->post_type != 'documentos') return false;
		
		if ($this->isCanceled()) :
			echo '<style>#publish,#publishing-action,#radio-document_categoriadiv {display:none}</style>';
		endif;
	 	
	 	if ( ! empty($this->getCurrentStatus()) && ! $this->isVerified() && ! $this->isCanceled()) :
	 		echo '<style>#publish,#publishing-action {display:none}</style>';
			echo '<div class="misc-pub-section misc-pub-post-status" style="display: block;float:left;width:100%">
				<input name="save" type="submit"  class="preview button"  id="NewPublish" value="Salvar Alterações" style="float:left">
			</div>';
		endif;

	 	if ($this->getCurrentStatus() != 'Em elaboração' && ! empty($this->getCurrentStatus())) :
			echo '<div class="misc-pub-section misc-pub-post-status" style="display: block;float:left;width:100%">
			<label>Versão:</label>
			<span><strong>'.$this->getCurrentVersion().'</strong></span>
			</div>';
		endif;
		
		if ( ! empty($this->getCurrentStatus())) :
			echo '<div class="misc-pub-section misc-pub-post-status" style="display: block;float:left;width:100%">
			<label>Status:</label>
			<span><strong>'.$this->getCurrentStatus().'</strong></span>
			</div>';
		endif;
	 	
	 }
	 
	/**
	* Metodo para redenrizar o botão para salvar o proximo status do documento
	*/ 
	public function buttonNextStatus()
	{
		global $post;
		
		if ($post->post_type != 'documentos') return false;
		
		$nextStatus = $this->getNextStatus();
		
		echo '<input type="hidden" name="nextStatus" id="idNextStatus" value="0" data-value="'.$nextStatus['id'].'">';
		
		$termsPost = wp_get_post_terms($this->getId(), 'document_status');
		
		if (null == $termsPost) return false;
		
		if ($this->isElaborate() || $this->isVerified()) :
			//Renderizando o textarea para o preenchimento da observação
			$this->renderTextareaObs();	
		endif;
			
			
		if ($nextStatus['textAction'] != 'end' && ! empty($nextStatus['textAction'])) :
			
			$function = 'submitPost()';
			if (isset($nextStatus['function']) && ! empty($nextStatus['function'])) :
				$function = $nextStatus['function'];
			endif;
			
			echo '<input 
					name="save" 
					type="button" 
					class="button button-primary button-large" 
					id="saveStatusDocument" 
					value="'.$nextStatus['textAction'].'" 
					style="width: 100%;"
					onclick="'.$function.'">';
		else:
			
			//echo '<div id="major-publishing-actions">';
			
			//Removendo o botão de salvar
			$this->removeSaveButton();
			
			//Verificando se o documento foi verificado
			if ($this->isVerified()) :
				//Renderizando o botão de reprovar o documento
				$this->renderButtonDisapproveDocument();
				//Renderizando o botão de aprovar o documento
				$this->renderButtonApproveDocument();
			endif;
			
			//Verificando se o documento foi aprovado
			if ($this->isApproved()) :
				//Renderizando o botão de cancelar o documento
				$this->renderButtonCancelDocument();
			endif;
			
			//Verificando se o documento foi cancelado
			if ($this->isCanceled()) :
				echo '<p><strong>Este documento está cancelado.</strong></p>';	
			endif;
			
			//Verificando se o documento foi reprovado ou aprovado
			if ($this->isDisapproved() || $this->isApproved()) :
				$this->renderRepeatEditFlow();
			endif;
			
		//	echo '</div>';
		endif;
	}
	/**
	 * Trocando o id owner das versoes para um novo id
	 */
	 public function updateOwnerVersion($newId)
	 {
	 	if (empty($newId)) return false;
	 	
		$versions = new WP_Query(array( 
		    'post_type' 	 => 'versao',
		    'posts_per_page' => -1,
		    'meta_key'  	 => 'version_order',
		    'orderby'   	 => 'meta_value', 
		    'meta_query'	 => array(
				'relation' => 'AND',
				array(
					'key'     => 'document_owner',
					'value'   => $GLOBALS['controlId'],
					'compare' => '='
				),
			),
		));
		
		if ( ! $versions->have_posts()) return false;
		
		wp_set_post_terms($GLOBALS['controlId'], $this->getStatusById('Elaborando nova versão'), 'document_status', true);
		
		$count = 0;
		while ($versions->have_posts()) : $versions->the_post();
		
			if ($count == 0) {
				$version = get_post_meta($GLOBALS['controlId'], 'version_order_ref', true);
				update_post_meta($newId, 'version_order_ref', $version);
			 	update_post_meta($newId, 'mainVersion', 	  $version);
			}
			$count = 1;
			
			update_post_meta(get_the_ID(), 'document_owner', $newId);
			
		endwhile;
	 }
	
	/**
	 * Metodo para criar um novo fluxo de avaliacao de um documento Aprovado ou Reprovado
	 */
	 public function newFlow($post_id, $post, $update)
	 {
	 
	 	if ($GLOBALS['control'] === 1) return;
            
	 	if ( ! isset($_POST['newFlow']) || $_POST['newFlow'] == 0) return false;
	
		$GLOBALS['control']++;
	 	
	 	$GLOBALS['controlId'] = $post_id;
	 	
	 	$postFlow = (array)$post;
		$main_id  = $postFlow['ID'];
		
		// Removendo oque não deve ser passado para o próximo post
		unset($postFlow['ID']);
		unset($postFlow['guid']);
		unset($postFlow['post_type']);
		unset($postFlow['post_date']);
		unset($postFlow['post_date_gmt']);
		unset($postFlow['post_date_gmt']);
		unset($postFlow['post_name']);
		unset($postFlow['post_modified']);
		unset($postFlow['post_modified_gmt']);
		
		// Adicionando novas informações
		$postFlow['post_title']	 = $postFlow['post_title'];
		$postFlow['post_type']	 = 'documentos'; 


		// Criando a versão
		$new_version = wp_insert_post($postFlow);
	 	
		//Trocando a referencia para o novo documento
		$this->updateOwnerVersion($new_version);
		
		// Referências para o documento de origem
		update_post_meta($new_version, 'document_owner_flow', $main_id);
		
		//Salvamento de metas personalizadas
		update_post_meta($new_version, 'version_order', 0.1);
		update_post_meta($new_version, 'user_save_id', get_current_user_id());
		
		//Salvando o status inicial ao documento
		$firstStatus = get_term_by('name',FIRST_STATUS, 'document_status');
		wp_set_post_terms($new_version, $firstStatus->term_id, 'document_status', true);
		
		//Salvando a mesma categoria do documento referenciado
		wp_set_post_terms($new_version, $this->getCategory($post_id), 'document_categoria', true);

		//Redirecionando para a listagem de documentos
		$url = admin_url().'edit.php?post_type=documentos';
		wp_redirect($url);
		die();
		
	 }
	 
	/**
	 * Metodo para salvar as versões do documento
	 */ 
	public function createVersion($ID, $post)
	{	
		
		if ($post->post_type != 'documentos') return false;
		
		//Setando o ID do documento
		$this->setId($ID);
		
		//Verificando se vai ter um novo fluxo de edicao de um documento aprovado ou reprovado
		if (isset($_POST['newFlow']) && $_POST['newFlow'] == 1)	return false;
		
		//Pegando o id do proximo status
		$nextStatus = ($_POST['nextStatus']) ? $_POST['nextStatus'] : 0;
		
		//Setando o proximo status
		$this->setNextStatus($nextStatus);
		
		// Setando a versao principal
		$this->setMainVersion();
		
		//Pegando o nome do status atual
		$nameStatus = wp_get_post_terms($this->getId(), 'document_status')[0]->name;
		
		//Se for o primeiro status, nao guarda versao
		if ($nextStatus == 0) return false;
		
		// Verificando se vai criar versao para o status passado
		//if ( ! $this->IsToCreateVersion($nameStatus)) return false;
	
		$postarr = (array)$post;
		$main_id = $postarr['ID'];
		
		// Removendo oque não deve ser passado para o próximo post
		unset($postarr['ID']);
		unset($postarr['guid']);
		unset($postarr['post_type']);
		unset($postarr['post_title']);
		
		// Adicionando novas informações
		$postarr['post_title']	= $post->post_title;
		$postarr['post_type'] 	= 'versao';
		
		// Criando a versão
		$new_version = wp_insert_post($postarr);
		
		// Referências para o documento de origem
		update_post_meta($new_version, 'document_owner', $main_id);
		
		//Salvamento de metas personalizadas
		if (null == get_post_meta($main_id, 'version_order_ref', true)) {
			$versionNumber = 0.01;
			update_post_meta($main_id, 'version_order_ref', $versionNumber);
		} else {
			$versionNumber = get_post_meta($main_id, 'version_order_ref', true);
			$versionNumber = $versionNumber + 0.01;
			
			if ($this->isApproved())
				$versionNumber = $this->getMainVersion();
				
			update_post_meta($main_id, 'version_order_ref', $versionNumber);
		}

		
		update_post_meta($new_version, 'version_order', $versionNumber);
		update_post_meta($new_version, 'user_save_id',	get_current_user_id());
		update_post_meta($new_version, 'status_id', 	$nextStatus);
		update_post_meta($new_version, 'status_name',	$nameStatus);
		update_post_meta($new_version, 'date',			date('d/m/Y'));
		update_post_meta($new_version, 'date_full',		date('d/m/Y H:i:s'));
		
		
		if ($this->isApproved())
			update_post_meta($new_version, 'versionMain', $this->getMainVersion());
		
		$obs = (isset($_POST['documentObs'])) ?  $_POST['documentObs'] : '';
		update_post_meta($new_version, 'obs', $obs);
		
		if ($this->isDisapproved()) :
			$this->newValidationFlow();
		endif;
	}
}
$document = new Document;
$document->setOrderStatus($orderStatus);