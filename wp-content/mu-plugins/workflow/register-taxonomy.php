<?php

/**
 * Categoria dos documentos
 */
function categoriaDosDocumentos()
{
	$labels = array(
		'name'              => _x( 'Categorias', 'taxonomy general name', 'textdomain' ),
		'singular_name'     => _x( 'Categoria', 'taxonomy singular name', 'textdomain' ),
		'search_items'      => __( 'Pesquisar categoria', 'textdomain' ),
		'all_items'         => __( 'Todas categorias', 'textdomain' ),
		'parent_item'       => __( 'Parent Origem', 'textdomain' ),
		'parent_item_colon' => __( 'Parent Origem:', 'textdomain' ),
		'edit_item'         => __( 'Editar categoria', 'textdomain' ),
		'update_item'       => __( 'Atualizar categoria', 'textdomain' ),
		'add_new_item'      => __( 'Adicionar nova categoria', 'textdomain' ),
		'new_item_name'     => __( 'Adicionar novo nome da categoria', 'textdomain' ),
		'menu_name'         => __( 'Categoria', 'textdomain' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array('slug' => 'document_categoria'),
	);

	register_taxonomy('document_categoria', array('documentos'), $args);
	
	$createDocumentCategoria = array(
		array(
			'name' => 'Documentação das certificações ISO e CMMI',
			'icon' => 'fa-file-text'
		),
		array(
			'name' => 'Ferramentas, Técnicas e Regras',
			'icon' => 'fa-wrench'
		),
		array(
			'name' => 'Instruções de Trabalho',
			'icon' => 'fa-files-o'
		),
		array(
			'name' => 'Manual da Qualidade',
			'icon' => 'fa-book'
		),
		array(
			'name' => 'Procedimentos',
			'icon' => 'fa-bar-chart'
		)
	);
	foreach($createDocumentCategoria as $categoria) {
		wp_insert_term(
			$categoria['name'],
			'document_categoria',
			array(
				'slug' 		  => sanitize_title($categoria['name']),
				'description' => $categoria['icon'],
			)
		);	
	}
}
add_action( 'init', 'categoriaDosDocumentos' );


/**
 * Status dos documentos
 */
function statusDosDocumentos()
{
	$labels = array(
		'name'              => _x( 'Status', 'taxonomy general name', 'textdomain' ),
		'singular_name'     => _x( 'Status', 'taxonomy singular name', 'textdomain' ),
		'search_items'      => __( 'Pesquisar status', 'textdomain' ),
		'all_items'         => __( 'Todas statuss', 'textdomain' ),
		'parent_item'       => __( 'Parent Origem', 'textdomain' ),
		'parent_item_colon' => __( 'Parent Origem:', 'textdomain' ),
		'edit_item'         => __( 'Editar status', 'textdomain' ),
		'update_item'       => __( 'Atualizar status', 'textdomain' ),
		'add_new_item'      => __( 'Adicionar nova status', 'textdomain' ),
		'new_item_name'     => __( 'Adicionar novo nome da status', 'textdomain' ),
		'menu_name'         => __( 'Status', 'textdomain' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => false,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'status' ),
	);

	register_taxonomy('document_status', array('documentos'), $args );
	
	$createStatus = array(
		'Aprovado',
		'Cancelado',
		'Elaborado',
		'Em elaboração',
		'Reprovado',
		'Verificado',
		'Elaborando nova versão'
	);
	foreach($createStatus as $status) {
		wp_insert_term(
			$status,
			'document_status',
			array(
				'slug' => sanitize_title($status)
			)
		);	
	}
}
add_action('init','statusDosDocumentos' );
