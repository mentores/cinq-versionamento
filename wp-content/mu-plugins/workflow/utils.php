<?php
/**
 * UTILS
 */
function is_documents(){
	if (
		(isset($_GET['post_type']) && $_GET['post_type'] == 'documentos') 
		|| 
		(isset($_GET['post']) && get_post_type($_GET['post']) == 'documentos')
		) 
		return true;
}
