<?php
/**
 * Template name: Qualidade Novo
 */
?>
<?php get_header(); ?>
	
<h1><?php the_title(); ?></h1>
<style>
#menuQualidade,
#menuQualidade + .menu-arrow.menu-right-arrow,
.menu-arrow.menu-left-arrow{
	display: none;
}
.listagem-documentos .highlighter {
	background: #05B8FF;
	width: 50px;
	height: 40px;
	float: left;
	line-height: 40px;
	text-align: center;
	color: white;
	border-radius: 5px;
	margin-top: 9px;
	margin-right: 10px;
	text-shadow: 1px 1px 1px rgba(0, 0, 0, 0.20);
}
.listagem-documentos .table {
    font-size: 16px;
    color: #585f6b;
}
.listagem-documentos ul li:nth-child(1) .highlighter {
	background: #29677F;
}
.listagem-documentos ul li:nth-child(2) .highlighter {
	background: #0483b5;
}
.listagem-documentos ul li:nth-child(3) .highlighter {
	background: #089fdc;
}
.listagem-documentos ul li:nth-child(4) .highlighter {
	background: #05B8FF;
}

.listagem-documentos .categoria-documentos ul{
	list-style: none;
	float: left;
	margin: 0px;
	padding: 0px;
	border: 1px solid rgba(204, 204, 204, 0.51);
	border-radius: 5px;
}
.listagem-documentos .categoria-documentos ul li {
	width: 100%;
	float: left;
	line-height: 60px;
	border-bottom: 1px solid rgba(204, 204, 204, 0.51);
}
.listagem-documentos .categoria-documentos ul li a {
    display: block;
    transition: .3s;
    text-decoration: none;
    color: #585f6b;
    padding-left: 15px;
    padding-top: 11px;
    padding-bottom: 11px;
    float: left;
    width: 100%;
}
.listagem-documentos .categoria-documentos ul li a:hover{
	background-color: rgba(204, 204, 204, 0.4);
}
</style>
<div class="row listagem-documentos">
	<div class="col-md-3 categoria-documentos">
	<?php 
	//wp_reset_query();

	$terms = get_terms(array(
		'taxonomy' => 'document_categoria',
		'hide_empty' => false,
	));

	echo '<pre>';

	print_r(get_taxonomy('document_categoria'));
	die();

	/**
		 * The WordPress Query class.
		 * @link http://codex.wordpress.org/Function_Reference/WP_Query
		 *
		 */
		$args = array(
			'post_type' => array(
				'post',
				'page',
				'revision',
				'attachment',
				'my-post-type',
				)
		);
	
	$query = new WP_Query( $args );
	
	?>
	<ul>
		<li>
			<a href="">
				<div class="highlighter">
					58
				</div>
				Todas as Pautas
			</a>
		</li>
		<li>
			<a href="">
				<div class="highlighter">
					4
				</div>
				Todas as Pautas
			</a>
		</li>
		<li>
			<a href="">
				<div class="highlighter">
					22
				</div>
				Todas as Pautas
			</a>
		</li>
		<li>
			<a href="">
				<div class="highlighter">
					58
				</div>
				Todas as Pautas
			</a>
		</li>
		<li>
			<a href="">
				<div class="highlighter">
					30
				</div>
				Todas as Pautas
			</a>
		</li>

	</ul>
	</div>
	<div class="col-md-9">
		<table class="table table-striped">
			<thead> 
				<tr>
					<th>Título</th> 
					<th>Criado em</th> 
					<th>Detalhes</th> 
				</tr> 
			</thead>
			<tr>
				<td><a href="#">Documento com um nome bem legal</a></td>
				<td>04/04/2018</td>
				<td>
					<a href="#">
						Ver
					</a>
				</td>
			</tr>
		</table>
	</div>
</div>

<?php get_footer(); ?>

