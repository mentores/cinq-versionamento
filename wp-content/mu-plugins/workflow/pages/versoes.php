<style type="text/css">
.update-nag,
#dwqa-message {
    display: none;
}
.versionPublish {
    background: #dff0d8 !important;
}
.versionPublish td {
    color: #3c763d !important;
}
.versionDisapproved {
    background-color: #fcf8e3 !important;
}
.versionDisapproved td {
    color: #8a6d3b !important;
}
</style>
<?php

    $role = get_userdata(get_current_user_id());
    global $post;

    $documentId = ($_GET['idDocument']) ? $_GET['idDocument'] : $post->ID;
    
    $query = new WP_Query(array( 
        'post_type'      => 'versao',
        'posts_per_page' => -1,
        'meta_key'       => 'version_order',
        'orderby'        => 'meta_value',
        'meta_query'     => array(
            'relation' => 'AND',
    		array(
    			'key'     => 'document_owner',
    			'value'   => $documentId,
    			'compare' => '='
    		),
    	),
    ));
  
if ( ! isset($post->ID)) :
?>
<div class="wrap">
    <h1 style="display: block;">
        <a href="<?php echo get_bloginfo('url')?>/wp-admin/edit.php?post_type=documentos" class="page-title-action">Voltar</a>
    </h1>
</div>
<?php 
endif;

if ($query->have_posts()) :
?>
<table class="wp-list-table widefat fixed striped" style="margin-top: 20px;">
    
    <thead>
        <tr>
            <td><strong>Versão</strong></td>
            <td><strong>Status</strong></td>
            <td><strong>Data</strong></td>
            <td><strong>Responsável</strong></td>
            <td><strong>Observação</strong></td>
            <td><strong>Detalhes</strong></td>
        </tr>
    </thead>
        
    <?php
    	while ($query->have_posts()) : $query->the_post();
    	
    	$status_name   = get_post_meta(get_the_ID(),'status_name', true);
    	$user_save_id  = get_post_meta(get_the_ID(),'user_save_id', true);
    	$user_obj      = get_user_by('id', $user_save_id);
    	$version_order = get_post_meta(get_the_ID(),'version_order', true);
    	$obs           = get_post_meta(get_the_ID(),'obs', true);
    	$data          = get_post_meta(get_the_ID(),'date', true);
    	$dataFull      = get_post_meta(get_the_ID(),'date_full', true);
    	
    	$versionMain   = (null == get_post_meta(get_the_ID(),'versionMain', true)) ? 0 : get_post_meta(get_the_ID(),'versionMain', true);
        $link          = admin_url().'/post.php?post='.get_the_ID().'&action=edit#document='.$documentId;
        ?>
        <tr <?php echo ($status_name == 'Reprovado') ? 'class="versionDisapproved"' : ''; echo ($versionMain) ? 'class="versionPublish"' : '' ?>>
            <td><a href="<?php echo $link;?>" target="_blank"><?php echo $version_order; ?></a></td>
            <td><?php echo $status_name; ?></td>
            <td><?php echo $data; ?></td>
            <td><?php echo $user_obj->user_login; ?></td>
            <td><?php echo $obs; ?></td>
            <td><a href="<?php echo $link;?>" target="_blank">Ver Documento</a></td>
        </tr>	
    <?php
    	endwhile;
    ?>
    
</table>
<?php 
else:
    echo '<h2>Nenhuma versão salva.</h2>';
endif;
?>

