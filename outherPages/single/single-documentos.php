﻿<?php get_header(); 

if ( ! have_posts()) : 
	echo '<p>'._e('Sorry, this post does not exist').'</p>';
endif;
the_post();

$mainVersion = get_post_meta(get_the_ID(), 'mainVersion', true);
?>
<div class="container-fluid">
	<ul id="menuQualidade" class="nav navbar-nav nav-quali menu-ul">
		<li>
			<a href="/manual-da-qualidade/">
				<i class="fa fa-2x fa-chevron-circle-left"></i> Voltar
			</a>
		</li>
	</ul>
</div>

<h1><?php the_title(); ?></h1>

<div class="col-md-9">   
	<?php the_content(); ?>
</div>

<div class="col-md-3" style="border: 1px solid #c1c1c1;padding: 14px 0px;">
	<div class="col-md-12">
		<label>Versão:</label>
		<span><strong><?php echo $mainVersion; ?></strong></span>
	</div>
	
	<div class="col-md-12">
		<hr style="border: 1px solid rgba(51, 51, 51, 0.3); margin: 10px 0px;">
	</div>

	<div class="col-md-12">
		<label>Publicado em:</label>
		<span><strong><?php the_time('d/m/Y') ?></strong></span>
	</div>
	<?php 
	$queryVersion = new WP_Query(array( 
        'post_type'      => 'versao',
        'posts_per_page' => 3,
        'meta_key'       => 'version_order',
        'orderby'        => 'meta_value',
        'meta_query'     => array(
            'relation' => 'AND',
    		array(
    			'key'     => 'document_owner',
    			'value'   => get_the_ID(),
    			'compare' => '='
    		),
    		array(
    			'key'     => 'version_order',
    			'value'   => $mainVersion,
    			'compare' => '<='
    		),
    	),
	));
	while ($queryVersion->have_posts()) : $queryVersion->the_post();

		$status_name   = get_post_meta(get_the_ID(),'status_name', true);
		$user_save_id  = get_post_meta(get_the_ID(),'user_save_id', true);
		$user_obj      = get_user_by('id', $user_save_id);
		$nameUser 	   = (get_user_meta($user_save_id, 'first_name', true)) ? get_user_meta($user_save_id, 'first_name', true) : $user_obj->user_login;

		echo '<div class="col-md-12">
				<label>'.$status_name.' por:</label>
				<span><strong>'.$nameUser.'</strong></span>
			 </div>';
	endwhile;
	?>
</div>
<?php 
get_footer();
?>