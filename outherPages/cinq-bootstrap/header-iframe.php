<?php $menutoggle = isset($_COOKIE['nav-toggle'])?!(bool)$_COOKIE['nav-toggle']:false;?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?php bloginfo('name'); ?> </title>

 
 
 <link href="<?php echo get_template_directory_uri();?>/style-teste.css" rel="stylesheet">
 
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

	
    <?php wp_enqueue_script('jquery'); ?>
    <?php wp_enqueue_script('masonry'); ?>
    
    
    <?php wp_head(); ?>

     <script type="text/javascript">
        
        jQuery(window).load(function() {
        
         // MASSONRY Without jquery
         var container = document.querySelector('.panel-grid-2');
         if(container)
            var msnry = new Masonry( container, {
              itemSelector: '.grid',
              columnWidth: '.col-md-4',                
            });  
          
   	   var container = document.querySelector('.panel-grid-3');
         if(container)
            var msnry = new Masonry( container, {
              itemSelector: '.grid',
              columnWidth: '.col-md-3',                
            });  
	  
	   
       
   
      });
		
		$j(document).ready(function() {
    $j(".dropdown-toggle").dropdown();
});
      
    </script>

</head>

<body>
<?php add_filter('show_admin_bar', '__return_false'); ?>
     
    
    <input class="nav-toggle <?php echo $menutoggle? 'nav-toggle-off' :''; ?>" type="checkbox" id="navigation" />
    <label class="nav-toggle <?php echo $menutoggle? 'nav-toggle-off' :''; ?>" for="navigation">
        <i class="fa fa-bars"></i>     
    </label>
    
    <div id="wrapper" class="<?php echo $menutoggle? 'wrapper-off' :''; ?>" >

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
				
				
               
			  
			   <a href="<?php echo esc_url( home_url() );?>" >
               <span class="logo <?php echo $menutoggle? 'logo-off' :''; ?> col-sm-8">                           
               </span></a>
			   
			   </div>
          
            <?php 
                $token    = get_user_meta( wp_get_current_user()->ID, 'token_access', true );

                $url_api  = rtrim(get_option('wpoa_api_url'), '/');
                $response = wp_remote_request(  $url_api."/Versao/GetVersaoModulos" , 
                             [  'method'   => 'GET',
                                'blocking' => true,
                                'headers'  => ['Authorization' => "Bearer $token"]] );
                $versoes = json_decode($response['body']);
            ?>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
              
                <li class="dropdown">  
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-question-circle"></i> 
                    </a>
                    <ul class="dropdown-menu message-dropdown">
                        <li class="message-preview"> Versão do sistema</li>
                        <?php foreach ($versoes as $versao) {?>
                        <li>
                            <span class="small text-muted">
                                <i class="fa fa-clock-o"></i>
                                <?php echo $versao->data;?>
                            </span> 
                            <?php echo $versao->modulo;?> - <?php echo $versao->versao;?>
                        </li>
                        <?php } ?>
                    </ul>
                </li>
                <li>
                    <i class="fa fa-user"></i> Bem-vindo,  <?php global $current_user;
										get_currentuserinfo();
										echo $current_user->display_name;?>!</li>
         
			
                   
                   
					
					<!---User Menu
			<ul class="dropdown-menu">
			 <?php wp_nav_menu( array('theme_location' => 'menu-usuario','menu_class' => '', 'menu' => '','container' => 'none','menu_id' => '',)); ?>
			<li class="divider"></li>
			
			<li>
                           
                        </li>
			</ul>---->
                     <li> 
                     <a href="<?php echo wp_logout_url( home_url() ); ?>"><i class="fa fa-fw fa-power-off"></i> Log Out</a> </li>

            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">



            <!--<input class="nav-toggle" type="checkbox" id="navigation" />
    <label class="nav-toggle" for="navigation">
        <i class="fa fa-bars"></i>
    </label>-->
                
                 <ul id="menu" class="nav navbar-nav side-nav <?php echo $menutoggle? 'side-nav-off' :''; ?>"> 

    <hr class="divider <?php echo $menutoggle? 'divider-off' :''; ?>">
     

                  
 
              <?php wp_nav_menu( array('theme_location' => 'menu-lateral','menu_class' => 'nav navbar-nav side-nav', 'menu' => '','container' => 'false','menu_id' => '',)); ?>
            
               </ul>
               
            </div>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

          

            <div class="container-fluid">
