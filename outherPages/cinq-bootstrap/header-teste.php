<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?php bloginfo('name'); ?> </title>

 
 
 <link href="<?php echo get_template_directory_uri();?>/style-teste.css" rel="stylesheet">
 
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

	
    <?php wp_enqueue_script("jquery"); ?>
    <?php wp_enqueue_script("masonry"); ?>
    <?php wp_head(); ?>


     <script type="text/javascript">
        
        jQuery(window).load(function() {
        
      // MASSONRY Without jquery
      var container = document.querySelector('.panel-grid-2');
      var msnry = new Masonry( container, {
        itemSelector: '.grid',
        columnWidth: '.col-md-4',                
      });  
      
	  
	  var container = document.querySelector('.panel-grid-3');
      var msnry = new Masonry( container, {
        itemSelector: '.grid',
        columnWidth: '.col-md-3',                
      });  
        });
      
    </script>

</head>

<body>
<?php add_filter('show_admin_bar', '__return_false'); ?>
     
    
    <input class="nav-toggle" type="checkbox" id="navigation" />
    <label class="nav-toggle" for="navigation">
        <i class="fa fa-bars"></i>     
    </label>
    
    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
				
				
               
			  
			   <span class="logo col-xs-8"><a href="<?php echo esc_url( home_url() );?>" ><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/logo-intranet.png"></a></span>
			   
			   </div>
          
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
                <!--<li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-envelope"></i> <b class="caret"></b></a>
                    <ul class="dropdown-menu message-dropdown">
                        <li class="message-preview">
                            <a href="#">
                                <div class="media">
                                    <span class="pull-left">
                                        <img class="media-object" src="http://placehold.it/50x50" alt="">
                                    </span>
                                    <div class="media-body">
                                        <h5 class="media-heading"><strong>John Smith</strong>
                                        </h5>
                                        <p class="small text-muted"><i class="fa fa-clock-o"></i> Yesterday at 4:32 PM</p>
                                        <p>Lorem ipsum dolor sit amet, consectetur...</p>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="message-preview">
                            <a href="#">
                                <div class="media">
                                    <span class="pull-left">
                                        <img class="media-object" src="http://placehold.it/50x50" alt="">
                                    </span>
                                    <div class="media-body">
                                        <h5 class="media-heading"><strong>
										<?php global $current_user;
										get_currentuserinfo();
										echo $current_user->user_firstname . ' ' . $current_user->user_lastname . "\n";?></strong>
                                        </h5>
                                        <p class="small text-muted"><i class="fa fa-clock-o"></i> Yesterday at 4:32 PM</p>
                                        <p>Lorem ipsum dolor sit amet, consectetur...</p>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="message-preview">
                            <a href="#">
                                <div class="media">
                                    <span class="pull-left">
                                        <img class="media-object" src="http://placehold.it/50x50" alt="">
                                    </span>
                                    <div class="media-body">
                                        <h5 class="media-heading"><strong>John Smith</strong>
                                        </h5>
                                        <p class="small text-muted"><i class="fa fa-clock-o"></i> Yesterday at 4:32 PM</p>
                                        <p>Lorem ipsum dolor sit amet, consectetur...</p>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="message-footer">
                            <a href="#">Read All New Messages</a>
                        </li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bell"></i> <b class="caret"></b></a>
                    
					
					<ul class="dropdown-menu alert-dropdown">
                        <li>
                            <a href="#">Alert Name <span class="label label-default">Alert Badge</span></a>
                        </li>
                        <li>
                            <a href="#">Alert Name <span class="label label-primary">Alert Badge</span></a>
                        </li>
                        <li>
                            <a href="#">Alert Name <span class="label label-success">Alert Badge</span></a>
                        </li>
                        <li>
                            <a href="#">Alert Name <span class="label label-info">Alert Badge</span></a>
                        </li>
                        <li>
                            <a href="#">Alert Name <span class="label label-warning">Alert Badge</span></a>
                        </li>
                        <li>
                            <a href="#">Alert Name <span class="label label-danger">Alert Badge</span></a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">View All</a>
                        </li>
                    </ul>
                </li>
				-->
				
                <li>
                    <i class="fa fa-user"></i> Bem-vindo,  <?php global $current_user;
										get_currentuserinfo();
										echo $current_user->display_name;?>!</li>
         
			
                   
                   
					
					<!---User Menu
			<ul class="dropdown-menu">
			 <?php wp_nav_menu( array('theme_location' => 'menu-usuario','menu_class' => '', 'menu' => '','container' => 'none','menu_id' => '',)); ?>
			<li class="divider"></li>
			
			<li>
                           
                        </li>
			</ul>---->
                     <li> 
                     <a href="<?php echo wp_logout_url( home_url() ); ?>"><i class="fa fa-fw fa-power-off"></i> Log Out</a> </li>

            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">



            <!--<input class="nav-toggle" type="checkbox" id="navigation" />
    <label class="nav-toggle" for="navigation">
        <i class="fa fa-bars"></i>
    </label>-->
                
                 <ul id="menu" class="nav navbar-nav side-nav"> 

    <hr class="divider">
     

                  
 
              <?php wp_nav_menu( array('theme_location' => 'menu-lateral','menu_class' => 'nav navbar-nav side-nav', 'menu' => '','container' => 'false','menu_id' => '',)); ?>
            
               </ul>
               
            </div>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

          

            <div class="container-fluid">