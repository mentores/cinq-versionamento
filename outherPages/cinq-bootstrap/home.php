<?php get_header(); ?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>


	<h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
<p><em><?php the_time('l, F jS, Y'); ?></em></p>	
	<?php the_content(); ?>

<?php endwhile; else: ?>
	<p><?php _e('Sorry, there are no posts.'); ?></p>
<?php endif; ?>

<?php get_footer(); ?>