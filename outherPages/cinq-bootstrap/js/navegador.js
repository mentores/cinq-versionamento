// browser compatibility: get method for event 
    // addEventListener(FF, Webkit, Opera, IE9+) and attachEvent(IE5-8)
    var myEventMethod =
        window.addEventListener ? "addEventListener" : "attachEvent";
    // create event listener
    var myEventListener = window[myEventMethod];
    // browser compatibility: attach event uses onmessage
    var myEventMessage =
        myEventMethod == "attachEvent" ? "onmessage" : "message";
    // register callback function on incoming message
    myEventListener(myEventMessage, function (e) {
        // we will get a string (better browser support) and validate
        // if it is an int - set the height of the iframe #my-iframe-id
        /*if (e.data === parseInt(e.data))
		{
            document.getElementById('test').height = e.data + "px";
		}*/
		  
		var jsonOutput = JSON.parse(e.data);
		
		if (typeof jsonOutput.altura === "number")
		{
            document.getElementById('test').height = jsonOutput.altura + "px";
            $j('.panel-grid-2').masonry();
		}
		if (typeof jsonOutput.zerarScroll === "boolean")
		{
			if (jsonOutput.zerarScroll == true)
			{
				document.scrollingElement.scrollTop = 0;
			}
		}
		
    }, false);
