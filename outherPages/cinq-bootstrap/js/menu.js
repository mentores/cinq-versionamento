jQuery( document ).ready(function() {
	/******************************
	 * Masonry */
	var masonryUpdate = function() {
	   setTimeout(function() {
         var container = document.querySelector('.panel-grid-2');
         if(container)
		      var msnry = new Masonry( container, {
		        itemSelector: '.grid',
		        columnWidth: '.col-md-4',                
		      });  
	      
		  
		  	var container = document.querySelector('.panel-grid-3');
		  	if(container)
		      var msnry = new Masonry( container, {
		        itemSelector: '.grid',
		        columnWidth: '.col-md-3',                
		      });  
	   }, 500);
	}
   	
	jQuery("input[type='checkbox'].nav-toggle").click(masonryUpdate);
	jQuery(document).on('click', masonryUpdate);
	jQuery(document).ajaxComplete(masonryUpdate);

	/******************************
	 * Menu */
	 
   jQuery("input[type='checkbox'].nav-toggle").click(function(){
	   	// console.log("FOI");
	   	if(jQuery(".nav-toggle").hasClass("nav-toggle-off"))
   			jQuery.cookie("nav-toggle", "1", { path: '/' });
		else
			jQuery.cookie("nav-toggle", "0", { path: '/' });
			
		menuStatus()
	})

   
	function menuStatus(){
		jQuery(".logo",".navbar-header").toggleClass("logo-off");
		jQuery(".nav-toggle")			  .toggleClass("nav-toggle-off");
		jQuery(".side-nav")				  .toggleClass("side-nav-off");
		jQuery(".divider")				  .toggleClass("divider-off");
		jQuery(".menu-item-text")		  .toggleClass("menu-item-text-off");
		jQuery("#wrapper") 				  .toggleClass("wrapper-off");
	}
	
 		
 	/******************************/

	// TODO: REMOVER DUPLICATAS.
    if (jQuery('#menuQualidade').length > 0) {
        bindArrow();
        if ((jQuery("#menuQualidade .current-menu-item").width() + parseInt(jQuery("#menuQualidade .current-menu-item").offset().left)) > jQuery("#menuQualidade").width()) {

            jQuery("#menuQualidade").scrollLeft(jQuery("#menuQualidade .current-menu-item").offset().left)
        }

        checkMenuScrollPosition();

		jQuery(window).resize(function() {
        	checkMenuScrollPosition();
	 	});
    } 
	// DUPLICADO POIS A MESMA AÇÃO DEVE SER FEITA EM DOIS LUGARES DIFERENTES, MAS NÃO ESTÁ COM CLASSES IGUAIS,
	//remover no futuro.
	if (jQuery('.solicitacoes-wrapper .menu-ul').length > 0) {
        bindArrow();
        if ((jQuery(".solicitacoes-wrapper .menu-ul .current-menu-item").width() + parseInt(jQuery(".solicitacoes-wrapper .menu-ul .current-menu-item").offset().left)) > jQuery(".solicitacoes-wrapper .menu-ul").width()) {

            jQuery(".solicitacoes-wrapper .menu-ul").scrollLeft(jQuery(".solicitacoes-wrapper .menu-ul .current-menu-item").offset().left)
        }

        checkMenuScrollPosition();

		jQuery(window).resize(function() {
        	checkMenuScrollPosition();
	 	});
    } 
});


function bindArrow() {
    jQuery(".quali-menu-arrow").on('click', function () {
        if (jQuery(this).hasClass("quali-menu-left-arrow")) {
            jQuery("#menuQualidade").animate({ scrollLeft: '-=500' }, '500', 'swing', function () {checkMenuScrollPosition();});
        }
        else {
            jQuery("#menuQualidade").animate({ scrollLeft: '+=500' }, '500', 'swing', function () { checkMenuScrollPosition(); });
        }
    });

    jQuery(".quali-menu-arrow").on('mouseout', function () {
    	var e = event.toElement || event.relatedTarget;
        if (e.parentNode == this || e == this) {
           return;
        }
        checkMenuScrollPosition();
    });

	//DUPLICADO COM CLASSE DIFERENTE, REMOVER NO FUTURO
	jQuery(".menu-arrow").on('click', function () {
        if (jQuery(this).hasClass("menu-left-arrow")) {
            jQuery(".solicitacoes-wrapper .menu-ul").animate({ scrollLeft: '-=500' }, '500', 'swing', function () {checkMenuScrollPosition();});
        }
        else {
            jQuery(".solicitacoes-wrapper .menu-ul").animate({ scrollLeft: '+=500' }, '500', 'swing', function () { checkMenuScrollPosition(); });
        }
    });

    jQuery(".menu-arrow").on('mouseout', function () {
    	var e = event.toElement || event.relatedTarget;
        if (e.parentNode == this || e == this) {
           return;
        }
        checkMenuScrollPosition();
    });

}

function checkMenuScrollPosition() {
	if(jQuery("#menuQualidade").length > 0)
	{
		if (jQuery("#menuQualidade").scrollLeft() == 0 && jQuery('.quali-menu-arrow:hover').length == 0) {
			jQuery(".quali-menu-left-arrow").hide();
		}
		else {
			jQuery(".quali-menu-left-arrow").show();
		}

		if ((jQuery("#menuQualidade")[0].scrollLeft == jQuery("#menuQualidade")[0].scrollWidth - jQuery("#menuQualidade").width()) && jQuery('.quali-menu-arrow:hover').length == 0) {
			jQuery(".quali-menu-right-arrow").hide();
		}
		else {
			jQuery(".quali-menu-right-arrow").show();
		}
	}
	//DUPLICADO, REMOVER NO FUTURO.
	if(jQuery(".solicitacoes-wrapper .menu-ul").length > 0)
	{
		if (jQuery(".solicitacoes-wrapper .menu-ul").scrollLeft() == 0 && jQuery('.menu-arrow:hover').length == 0) {
			jQuery(".menu-left-arrow").hide();
		}
		else {
			jQuery(".menu-left-arrow").show();
		}

		if ((jQuery(".solicitacoes-wrapper .menu-ul")[0].scrollLeft == jQuery(".solicitacoes-wrapper .menu-ul")[0].scrollWidth - jQuery(".solicitacoes-wrapper .menu-ul").width()) && jQuery('.menu-arrow:hover').length == 0) {
			jQuery(".menu-right-arrow").hide();
		}
		else {
			jQuery(".menu-right-arrow").show();
		}
	}
}
