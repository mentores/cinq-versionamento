<?php

?>




             <div class="container-fluid">
  
  <ul class="nav navbar-nav nav-quali">
  <li class="active"><a href="https://intra-dev.cinq.com.br/docs-qualidade/manual-da-qualidade-on-iso/"><i class="fa fa-check-circle"></i><br/>Manual da Qualidade on ISO</a></li>
  <li><a href="https://intra-dev.cinq.com.br/documentos-da-qualidade/procedimentos-on-iso/"><i class="fa fa-list-alt"></i><br/>Procedimentos on ISO</a></li>
  <li><a href="https://intra-dev.cinq.com.br/documentos-da-qualidade/instrucoes-de-trabalho-on-iso/"><i class="fa fa-info-circle"></i><br/>Instruções de Trabalho on ISO</a></li>
  <li><a href="https://intra-dev.cinq.com.br/documentos-da-qualidade/ferramentas-tecnicas-e-regras-on-iso/"><i class="fa fa-wrench"></i><br/>Ferramentas, Técnicas e Regras on ISO</a></li>
  <li><a href="https://intra-dev.cinq.com.br/documentos-da-qualidade/glossario/"><i class="fa fa-book"></i><br/>Glossário</a></li>
  </ul></div>
                    