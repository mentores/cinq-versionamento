<?php /* Template Name: Assinatura */ ?>

<?php get_header(); ?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

<ul class="nav navbar-nav nav-quali"> 
<?php wp_nav_menu( array('theme_location' => 'menu-rh','menu_class' => 'nav navbar-nav nav-quali', 'menu' => '','container' => 'ul','menu_id' => '',)); ?>
</ul>
 
 <h1><?php the_title(); ?></h1>

<div class="col-md-8">
	
	
	
	<form name="formAssinatura" method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
	
		<div class="row">
			<div class="col-sm-6">
				<div class="form-group">
					<label for="Idioma">Idioma*</label>
					<div class="input-group">
						<div class="col-sm-6">
							<div class="radio">
							  <label>
								<input type="radio" name="in" id="radio" value="portugues" class="radio-language" checked>
							   Português
							  </label>
							  
							</div>
						 </div>
						 <div class="col-sm-6">
							<div class="radio">
							  <label>
								<input type="radio" name="in" id="radio" value="ingles" class="radio-language">
							   Inglês
							  </label>
							</div>
						 </div>
					</div>
				</div>
			</div>
		</div>
	
				
		<div class="row">
			<div class="col-sm-6">
				<div class="form-group">
					<label for="Nome">Nome</label>
					<div class="input-group">
						<div class="input-group-addon"><i class="fa fa-user"></i></div>
						<input type="text" class="form-control" name="nome" id="nome" value="<?php global $current_user; get_currentuserinfo(); echo $current_user->display_name;?>" placeholder="<?php global $current_user; get_currentuserinfo(); echo $current_user->display_name;?>">
					 </div>
				 
				</div>
			</div>
			
			<div class="col-sm-6">
				<div class="form-group">
					<label for="Nome">Email</label>
					<div class="input-group">
						<div class="input-group-addon"><i class="fa fa-envelope"></i></div>
						<input type="text" class="text-right form-control" name="email" id="email" value="<?php global $current_user; get_currentuserinfo(); echo $current_user->user_login . "\n";?>" placeholder="<?php global $current_user; get_currentuserinfo(); echo $current_user->user_login . "\n";?>">
						<div class="input-group-addon">@cinq.com.br</div>
					 </div>
				 
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6">
				<div class="form-group">
					<label for="Cargo">Cargo*</label>
					<div class="input-group">
						<div class="input-group-addon"><i class="fa fa-suitcase"></i></div>
						<input type="text" class="form-control" name="cargo" id="cargo" required>
					</div>
				 </div>
			</div>
			
			<div class="col-sm-6">
				<div class="form-group">
					<label for="Setor">Setor*</label>
					<select class="form-control setor-portugues" name="setor" id="setor" >
					  <option value="">Selecione</option>
					  <option value="1">Software Delivery Organization</option>
					  <option value="2">Marketing & Vendas</option>
					  <option value="3">Gestão & Professional Services</option>
					  <option value="4">Gestão</option>
					  <option value="5">Financeiro</option>
					  <option value="6">Recursos Humanos</option>
					  <option value="7">Governo</option>
					  <option value="8">Professional Services</option>
					  <option value="9">Marketing</option>
					  <option value="10">Vendas</option>
					  <option value="11">Recrutamento e Seleção</option>
					  <option value="12">Infraestrutura</option>
					  <option value="13">Qualidade</option>
					  <option value="14">Controladoria e Planejamento</option>
					  <option value="15">Sistemas Internos</option>
					</select>
					<select class="form-control setor-ingles" name="setor2" id="setor2" style="display: none;" >
					  <option value="">Selecione</option>
					  <option value="1">Software Delivery Organization</option>
					  <option value="2">Marketing & Sales</option>
					  <option value="3">Management & Professional Services</option>
					  <option value="4">Management</option>
					  <option value="5">Finance</option>
					  <option value="6">Human Resources</option>
					  <option value="7">Government</option>
					  <option value="8">Professional Services</option>
					  <option value="9">Marketing</option>
					  <option value="10">Sales</option>
					  <option value="11">Recruitment</option>
					  <option value="12">Infrastructure</option>
					  <option value="13">Quality</option>
					  <option value="14">Planning and Auditing</option>
					  <option value="15">Internal Systems</option>
					</select>
				 </div>
			</div>
			
		</div>
		
		<div class="row">
			<div class="col-sm-6">
				<div class="form-group">
					<label for="Setor">Telefone (ramal)*</label>
					<div class="input-group">
						<div class="input-group-addon"><i class="fa fa-phone"></i></div>
						<input type="text" class="form-control" name="ramal" id="ramal" data-mask="(__) ____-____" required>
					</div>
				 </div>
			</div>
			<div class="col-sm-6">
				<div class="form-group">
					<label for="Cargo">Telefone secundário (opcional)</label>
					<div class="input-group">
						<div class="input-group-addon"><i class="fa fa-mobile"></i></div>
						<input type="text" class="form-control" name="celular" id="celular" value="" data-mask="(__) ____-____">
					</div>					
				 </div>
			</div>
		</div>		
		
		<div class="row">
			<div class="col-sm-6">
				<label>
					<input type="checkbox" value="yes" name="scrum"> Scrum Master
				</label>
			</div>
			<div class="col-sm-6 text-right">
				 <input class="btn btn-info" type="submit" onclick="return validarCamposTelefone();"  name="submit" value="Atualizar assinatura">
			</div>
			
		</div>
 
  </form>
  

	
	
		<?php
	if(isset($_POST['submit'])) 
	{ 
	$nome = $_POST['nome'];
	$email = $_POST['email'];
	$cargo = $_POST['cargo'];
	$setor = $_POST['setor'];
	$ramal = $_POST['ramal'];
	$celular = $_POST['celular'];
	include 'assinatura-resultado.php';
	}
	?>

	<div class="row">
		
		<br/>
	<?php the_content(); ?>
		
	</div>

</div>
	


<?php endwhile; else: ?>
	<p><?php _e('Sorry, this post does not exist.'); ?></p>
<?php endif; ?>
       
<?php get_footer(); ?>