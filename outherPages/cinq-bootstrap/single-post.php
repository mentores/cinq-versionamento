<?php get_header(); ?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

<p class="alignright"><i class="fa fa-calendar"></i> <?php the_time('l, j F, Y, H:i') ?></p>

	<h1><?php the_title(); ?></h1>




<div class="col-md-9">

    <div class="thumbnail"> <?php if ( has_post_thumbnail() ) {
    the_post_thumbnail('large');
} ?> </div>

    
	<?php the_content(); ?>

<hr>
<?php comments_template(); ?> 
<?php endwhile; else: ?>
	<p><?php _e('Sorry, this post does not exist.'); ?></p>
<?php endif; ?>
        </div>
<?php get_footer(); ?>