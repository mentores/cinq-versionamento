<?php
/**
 * Template name: Qualidade Novo
 */
?>
<?php get_header(); ?>
<style>
#menuQualidade li a {
	font-size: 15px !important;
}
</style>
<script>
function showByCategory(obj, categorySlug, categoryName)
{	
	jQuery('.clickCategory').removeClass('current-menu-item');
	jQuery(obj).addClass('current-menu-item');

	setTitle(categoryName);

	jQuery('.table .list-query').hide();
	jQuery('.table .'+categorySlug).show();
}
function setTitle(title)
{
	jQuery('#category-title').html(title);
}
</script>
<?php 
$args = array(
	'post_type'   		=> 'documentos',
	'order'             => 'DESC',
	'orderby'           => 'date',
	'posts_per_page' 	=> -1,
	'tax_query' 		=> array(
		array(
			'taxonomy' => 'document_status',
			'field'    => 'name',
			'terms'    => 'Aprovado',
		),
		array(
			'taxonomy' => 'document_status',
			'field'    => 'name',
			'terms'    => 'Elaborando nova versão',
			'operator' => 'NOT IN'
		),
 	)
);
$queryDocumentos = new WP_Query($args);
$categorys = array();
if ($queryDocumentos->have_posts()) :
	$count 		   	   = 1;
	$firstCategory 	   = null;
	$firstCategorySlug = null;
	while($queryDocumentos->have_posts()) : $queryDocumentos->the_post();
		$terms = wp_get_post_terms(get_the_ID(), 'document_categoria')[0];

		if (isset($categorys[$terms->term_id])) :
			$categorys[$terms->term_id]['count']++;
		else:
			$categorys[$terms->term_id] = 
			array(
				'name'  		=> $terms->name,
				'count' 		=> 1,
				'slug'			=> $terms->slug,
				'description'	=> $terms->description
			);
		endif;
		if ($count ==1) {
			$firstCategory 	   = $terms->name;
			$firstCategorySlug = $terms->slug;
		}

		$count++;
	endwhile;
endif;
wp_reset_query();
?>
<div class="container-fluid">
	<ul id="menuQualidade" class="nav navbar-nav nav-quali menu-ul">
	<?php
	$count = 1;
	foreach ($categorys as $category) { 
		$category = (object)$category; 

		$icon = 'fa fa-book fa-2x';
		if ( ! empty($category->description))
			$icon = 'fa fa-2x '.$category->description;
		?>
		<li onclick="showByCategory(this, '<?php echo $category->slug ?>', '<?php echo $category->name ?>')" 
			class="clickCategory <?php echo ($count == 1) ? 'current-menu-item' : '' ?>">
			<a href="#">
				<i class="<?php echo $icon;?>"></i>
				<?php echo $category->name; ?>
			</a>
		</li>
	<?php
		$count++;
	}
	?>
	</ul>
</div>

<h1 id="category-title"><?php echo $firstCategory; ?></h1>

<div class="row listagem-documentos">
	<div class="col-md-12">
		<table class="table">
		<thead> 
			<tr>
				<th>Título</th> 
				<th>Categoria</th> 
				<th>Criado em</th> 
				<th>Detalhes</th> 
			</tr> 
		</thead>
		<?php 
		$args = array(
			'post_type'   		=> 'documentos',
			'order'             => 'DESC',
			'orderby'           => 'date',
			'posts_per_page' 	=> -1,
			'tax_query' 		=> array(
	      		array(
	      			'taxonomy' => 'document_status',
	      			'field'    => 'name',
	      			'terms'    => 'Aprovado',
	      		),
	      		array(
	      			'taxonomy' => 'document_status',
	      			'field'    => 'name',
	      			'terms'    => 'Elaborando nova versão',
	      			'operator' => 'NOT IN'
	      		),
	      	)
		);
		$queryDocumentos = new WP_Query($args);

		if ($queryDocumentos->have_posts()) :
			while($queryDocumentos->have_posts()) : $queryDocumentos->the_post();

			$terms  = wp_get_post_terms(get_the_ID(), 'document_categoria')[0];
			$hidden = ($firstCategorySlug != $terms->slug) ? 'style="display:none"' : '';
			?>
			<tr class="<?php echo $terms->slug?> list-query" <?php echo $hidden; ?>>
				<td><a href="<?php echo get_permalink(); ?>"><?php echo get_the_title(); ?></a></td>
				<td><?php echo $terms->name; ?></td>
				<td><?php echo get_the_date('d/m/Y'); ?></td>
				<td><a href="<?php echo get_permalink(); ?>">Ver</a></td>
			</tr>
			<?php
			endwhile;
		endif;
		?>
		</table>
	</div>
</div>

<?php get_footer(); ?>

